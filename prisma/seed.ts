import { PrismaClient, Sex } from '@prisma/client';
import * as bcrypt from 'bcrypt';

const prisma = new PrismaClient();

async function main() {
  console.log('Seeding database ...');

  console.log('Creating UserTypes ...');
  await prisma.userTypes.createMany({
    data: [
      { name: 'Gynecologue' },
      { name: 'Standard' },
      { name: 'Admin' },
      { name: 'Père Éducateur' },
    ],
    skipDuplicates: true,
  });
  console.log('Creating UserTypes : Done ✅');

  console.log('Creating NewsType ...');
  await prisma.newsTypes.createMany({
    data: [{ name: 'Gyneco' }, { name: 'Puberté' }, { name: 'Cardio' }],
    skipDuplicates: true,
  });
  console.log('Creating NewsType : Done ✅');

  // Stp au niveau de dotomi cree un seeder qui cree un user de type admin met mes info et ce mail : nobelagossa@gmail.com tel: 91271650

  console.log('Creating User ...');
  const admin = await prisma.userTypes.findUnique({
    where: {
      name: 'Admin',
    },
  });

  if (admin) {
    const password = await bcrypt.hash('nobelagossa', 10);

    await prisma.users.create({
      data: {
        email: 'nobelagossa@gmail.com',
        firstname: 'Nobel',
        name: 'Agossa',
        phoneNumber: '91271650',
        userTypeId: admin.id,
        login: 'nobelagossa',
        password: password,
        active: true,
        sex: Sex.MALE,
      },
    });
  }
  console.log('Creating User : Done ✅');
}

// execute the main function
main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
