/*
  Warnings:

  - The values [Initiated,Pending,Completed,Failed] on the enum `Status` will be removed. If these variants are still used in the database, this will fail.
  - A unique constraint covering the columns `[aggregatorId,aggregatorReference]` on the table `transactions` will be added. If there are existing duplicate values, this will fail.

*/
-- AlterEnum
BEGIN;
CREATE TYPE "Status_new" AS ENUM ('INITIATED', 'PENDING', 'COMPLETED', 'FAILED');
ALTER TABLE "transactions" ALTER COLUMN "status" TYPE "Status_new" USING ("status"::text::"Status_new");
ALTER TYPE "Status" RENAME TO "Status_old";
ALTER TYPE "Status_new" RENAME TO "Status";
DROP TYPE "Status_old";
COMMIT;

-- AlterTable
ALTER TABLE "users" ADD COLUMN     "remainingCredit" INTEGER NOT NULL DEFAULT 0;

-- CreateIndex
CREATE UNIQUE INDEX "transactions_aggregatorId_aggregatorReference_key" ON "transactions"("aggregatorId", "aggregatorReference");
