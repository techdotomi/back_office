/*
  Warnings:

  - A unique constraint covering the columns `[userId,gynecoId]` on the table `chat_rooms` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `senderId` to the `messages` table without a default value. This is not possible if the table is not empty.
  - Changed the type of `type` on the `messages` table. No cast exists, the column would be dropped and recreated, which cannot be done if there is data, since the column is required.

*/
-- CreateEnum
CREATE TYPE "MessageType" AS ENUM ('TEXT', 'IMAGE', 'AUDIO', 'VIDEO');

-- AlterTable
ALTER TABLE "messages" ADD COLUMN     "mediaUrl" TEXT,
ADD COLUMN     "senderId" TEXT NOT NULL,
DROP COLUMN "type",
ADD COLUMN     "type" "MessageType" NOT NULL,
ALTER COLUMN "content" DROP NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "chat_rooms_userId_gynecoId_key" ON "chat_rooms"("userId", "gynecoId");

-- AddForeignKey
ALTER TABLE "messages" ADD CONSTRAINT "messages_senderId_fkey" FOREIGN KEY ("senderId") REFERENCES "users"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
