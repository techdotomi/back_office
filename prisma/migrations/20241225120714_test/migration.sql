-- DropForeignKey
ALTER TABLE "chat_rooms" DROP CONSTRAINT "chat_rooms_initiatorId_fkey";

-- DropForeignKey
ALTER TABLE "chat_rooms" DROP CONSTRAINT "chat_rooms_participantId_fkey";

-- DropIndex
DROP INDEX "chat_rooms_initiatorId_participantId_key";
