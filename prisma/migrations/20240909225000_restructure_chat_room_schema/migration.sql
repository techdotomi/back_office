/*
  Warnings:

  - You are about to drop the column `gynecoId` on the `chat_rooms` table. All the data in the column will be lost.
  - You are about to drop the column `userId` on the `chat_rooms` table. All the data in the column will be lost.
  - A unique constraint covering the columns `[initiatorId,participantId]` on the table `chat_rooms` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `initiatorId` to the `chat_rooms` table without a default value. This is not possible if the table is not empty.
  - Added the required column `participantId` to the `chat_rooms` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "chat_rooms" DROP CONSTRAINT "chat_rooms_gynecoId_fkey";

-- DropForeignKey
ALTER TABLE "chat_rooms" DROP CONSTRAINT "chat_rooms_userId_fkey";

-- DropIndex
DROP INDEX "chat_rooms_userId_gynecoId_key";

-- AlterTable
ALTER TABLE "chat_rooms" DROP COLUMN "gynecoId",
DROP COLUMN "userId",
ADD COLUMN     "initiatorId" TEXT NOT NULL,
ADD COLUMN     "participantId" TEXT NOT NULL;

-- CreateIndex
CREATE UNIQUE INDEX "chat_rooms_initiatorId_participantId_key" ON "chat_rooms"("initiatorId", "participantId");

-- AddForeignKey
ALTER TABLE "chat_rooms" ADD CONSTRAINT "chat_rooms_initiatorId_fkey" FOREIGN KEY ("initiatorId") REFERENCES "users"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "chat_rooms" ADD CONSTRAINT "chat_rooms_participantId_fkey" FOREIGN KEY ("participantId") REFERENCES "users"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
