# Dotomi API

## Tech Stack

- Nest.js
- Prisma
- PostgreSQL

## Running locally

1. Install dependencies using npm

   ```bash
   npm install
   ```
2. Create the postgreSQL Database for this project 


3. Copy the `.env.example` to `.env` and update the variables.

   ```bash
   cp .env.example .env
   ```
4. Generate the Prisma client

   ```bash
   npm run prisma:generate
   ```

5. Push the database schema

   ```bash
   npm run prisma:push
   ```

6. Start the development server

   ```bash
   npm run start:dev
   ```
Then go to 127.0.0.1:3000/api or (127.0.0.1:PORT/api with PORT defined in the .env.development.local file) to access the documentation

