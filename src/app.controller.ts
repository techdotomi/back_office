import { Controller, Get, HttpCode, HttpStatus } from '@nestjs/common';
import { AppService } from './app.service';
// import { MailerService } from '@nestjs-modules/mailer';
import * as nodemailer from 'nodemailer';
import { ConfigService } from '@nestjs/config';

@Controller()
export class AppController {
  private readonly transporter: nodemailer.Transporter;

  constructor(
    private readonly appService: AppService,
    private readonly configService: ConfigService,
  ) {
    this.transporter = nodemailer.createTransport({
      host: configService.get<string>('EMAIL_HOST'),
      port: parseInt(configService.getOrThrow<string>('EMAIL_PORT')) || 2525,
      auth: {
        user: configService.get<string>('EMAIL_USERNAME'),
        pass: configService.get<string>('EMAIL_PASSWORD'),
      },
      secure: false,
      tls: {
        rejectUnauthorized: false,
      },
    });
  }

  /**
   * Health status.
   */
  @Get('health')
  @HttpCode(HttpStatus.OK)
  async ping(): Promise<any> {
    // const emailIsOkay = await this.testConnection();
    // console.log('emailIsOkay:', emailIsOkay);

    const services = {
      // email: emailIsOkay
      //   ? 'Email connection successful !'
      //   : 'Email connection failed !',
      server: 'Server is ok !',
    };
    return { status: 200, services };
  }

  async testConnection(): Promise<boolean> {
    try {
      return await this.transporter.verify();
    } catch (error) {
      console.log(`Connection failed: ${error.message}`);
      return false;
    }
  }
}
