import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { BadRequestException, ValidationPipe } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';
import { useContainer, ValidationError } from 'class-validator';
import { join } from 'path';
import { existsSync, mkdirSync } from 'fs';
import * as express from 'express';
import helmet from 'helmet';

async function bootstrap() {
  const confService = new ConfigService();

  const app = await NestFactory.create(AppModule);

  app.enableCors({
    origin: '*',
    methods: ['GET', 'HEAD', 'PUT', 'PATCH', 'POST', 'DELETE'],
    credentials: true,
    maxAge: 3600,
    allowedHeaders: ['Content-Type', 'Authorization'],
  });

  // For security purpose: helmet included many middleware functions to set security-related HTTP headers
  app.use(helmet());

  const port = confService.get<number>('PORT') || 3000;

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      exceptionFactory: (validationErrors: ValidationError[] = []) => {
        console.log('validationErrors:', validationErrors)
        return new BadRequestException(
          validationErrors.map((error) => ({
            field: error.property,
            error: Object.values(error.constraints!).join(', '),
          })),
        );
      },
    }),
  );

  // For use Dependency Injection in class-validator
  useContainer(app.select(AppModule), { fallbackOnErrors: true });

  const config = new DocumentBuilder()
    .setTitle('Dotomi API')
    .setDescription('The Dotomi API description')
    .setVersion('1.0')
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  // For read uploaded file

  const uploadDir = join(process.cwd(), 'uploads');
  if (!existsSync(uploadDir)) {
    mkdirSync(uploadDir);
  }

  app.use('/uploads', express.static(join(process.cwd(), 'uploads')));

  // For read public file

  const publicDir = join(process.cwd(), 'public');
  if (!existsSync(publicDir)) {
    mkdirSync(publicDir);
  }

  app.use('/public', express.static(join(process.cwd(), 'public')));

  await app.listen(port);
}
bootstrap()
  .then(() =>
    console.log(`Server is running on port ${process.env.PORT || 3000}`),
  )
  .catch((e) => console.error(e));
