import {
    IsIn,
    IsInt,
    IsOptional,
    Min,
  } from "class-validator";
  import { Type } from "class-transformer";
  
  export class PaginationDto {
    @IsOptional()
    @Type(() => Number)
    @IsInt()
    @Min(1)
    page?: number;
  
    @IsOptional()
    @Type(() => Number)
    @IsInt()
    @Min(1)
    pageSize?: number;
  }
  
  export class SortingDto {
    @IsOptional()
    @IsIn(["asc", "desc"])
    sortOrder?: "asc" | "desc" = "desc";
  }
  