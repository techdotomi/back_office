import { Module } from '@nestjs/common';
import { HandlebarsAdapter } from '@nestjs-modules/mailer/dist/adapters/handlebars.adapter';

import { AppController } from './app.controller';
import { AppService } from './app.service';

import { ConfigModule, ConfigService } from '@nestjs/config';
import { PrismaModule } from './libs/prisma/prisma.module';
import { UsersModule } from './modules/users/users.module';
import { GynecologistInfosModule } from './modules/gynecologist_infos/gynecologist_infos.module';
import { FatherEducatorInfosModule } from './modules/father_educator_infos/father_educator_infos.module';
import { UserTypesModule } from './modules/user_types/user_types.module';
import { AuthModule } from './modules/auth/auth.module';
import { TokenModule } from './modules/token/token.module';
import { TokenService } from './modules/token/token.service';
import { EmailService } from './modules/email/email.service';
import { EmailModule } from './modules/email/email.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { NewsModule } from './modules/news/news.module';
import { NewsTypesModule } from './modules/news_types/news_types.module';
import { FileUploadModule } from './modules/file_upload/file_upload.module';
import { CarrouselsModule } from './modules/carrousels/carrousels.module';
import { EventsModule } from './modules/events/events.module';
import { PackagesModule } from './modules/packages/packages.module';
import { SubscriptionsModule } from './modules/subscriptions/subscriptions.module';
import { AggregatorModule } from './modules/aggregator/aggregator.module';
import { TransactionsModule } from './modules/transactions/transactions.module';
import { ChatRoomGateway } from './modules/chat_room/chat_room.gateway';
import { ChatRoomModule } from './modules/chat_room/chat_room.module';
import { MessageModule } from './modules/message/message.module';
import { ChatRoomController } from './modules/chat_room/chat_room.controller';
import { NotificationsModule } from './modules/notifications/notifications.module';
import { NotificationsService } from './modules/notifications/notifications.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: `.env`,
      isGlobal: true,
    }),
    MailerModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        transport: {
          host: configService.get<string>('EMAIL_HOST'),
          port:
            parseInt(configService.getOrThrow<string>('EMAIL_PORT')) || 2525,
          auth: {
            user: configService.get<string>('EMAIL_USERNAME'),
            pass: configService.get<string>('EMAIL_PASSWORD'),
          },
          secure: false,
          tls: {
            rejectUnauthorized: false,
          },
        },
        defaults: {
          from: `No Reply ${configService.get<string>('EMAIL_USERNAME')}`,
        },
        template: {
          dir: process.cwd() + '/src/modules/email/templates',
          adapter: new HandlebarsAdapter(),
          options: {
            strict: true,
          },
        },
        preview: true,
      }),
      inject: [ConfigService],
    }),
    FileUploadModule,
    AuthModule,
    PrismaModule,
    UsersModule,
    GynecologistInfosModule,
    FatherEducatorInfosModule,
    GynecologistInfosModule,
    FatherEducatorInfosModule,
    UserTypesModule,
    TokenModule,
    EmailModule,
    NewsModule,
    NewsTypesModule,
    CarrouselsModule,
    EventsModule,
    PackagesModule,
    SubscriptionsModule,
    AggregatorModule,
    TransactionsModule,
    ChatRoomModule,
    MessageModule,
    // NotificationsModule,
  ],
  controllers: [AppController, ChatRoomController],
  providers: [
    AppService,
    TokenService,
    EmailService,
    ChatRoomGateway,
    // NotificationsService,
  ],
})
export class AppModule {}
