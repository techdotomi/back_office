import { Module } from '@nestjs/common';
import { TransactionsService } from './transactions.service';
import { TransactionsController } from './transactions.controller';
import { TransactionIsCompletedOrUsedRule } from './dto/validation-rules/transaction-is-completed-or-used.rule';
import { NotificationsService } from '../notifications/notifications.service';

@Module({
  controllers: [TransactionsController],
  providers: [
    TransactionsService,
    TransactionIsCompletedOrUsedRule,
    NotificationsService,
  ],
})
export class TransactionsModule {}
