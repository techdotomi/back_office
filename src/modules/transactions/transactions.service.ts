import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { UpdateTransactionDto } from './dto/update-transaction.dto';
import { PrismaService } from 'src/libs/prisma/prisma.service';
import { UUID } from 'crypto';
import { NotificationsService } from '../notifications/notifications.service';
import { Request } from 'express';
import { User } from '../users/entities/user.entity';
import { PaginationDto } from 'src/common/dto/pagination.dto';
import { SortingTransactionsDto } from './dto/sorting-transactions.dto';

@Injectable()
export class TransactionsService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly notificationsService: NotificationsService,
  ) {}

  private readonly logger = new Logger(TransactionsService.name);

  async create(createTransactionDto: CreateTransactionDto, req: Request) {
    const userId = (req.user as User).id as UUID;

    try {
      await this.findByAggregatorRefAndAggregatorId(
        createTransactionDto.aggregatorReference,
        createTransactionDto.aggregatorId,
      );
    } catch (error) {
      if (error instanceof NotFoundException) {
        const transaction = await this.prisma.transactions.create({
          data: {
            ...createTransactionDto,
            extra: createTransactionDto.extra
              ? createTransactionDto.extra
              : undefined,
            userId,
          },
          include: {
            user: {
              select: {
                id: true,
                name: true,
                firstname: true,
                UserType: {
                  select: {
                    name: true,
                  },
                },
              },
            },
          },
        });

        if (!transaction) {
          this.logger.log('Transaction could not be created');
          throw new BadRequestException('Transaction could not be created');
        }

        // const notification = await this.notificationsService.create({
        //   userId,
        //   data: {
        //     title: 'Transaction created',
        //     message: `Transaction with reference ${transaction.aggregatorReference} has been created`,
        //     type: 'transaction',
        //     reference: transaction.id,
        //   },
        //   active: true,
        // });
        const notification =  await this.notificationsService.create({
          userId: userId as UUID,
          active: true,
          data: {
            message: `Transaction with reference ${transaction.aggregatorReference} has been created`,
          },
        });

        if (!notification) {
          this.logger.log('Notification could not be created');
          // throw new BadRequestException('Notification could not be created');
        }
        return transaction;
      }
    }

    throw new BadRequestException([
      {
        field: 'aggregatorReference',
        message: 'Try another reference',
      },
    ]);
  }

  async findAll(
    paginationDto?: PaginationDto,
    sortingDto?: SortingTransactionsDto
  ) {

    const meta = {
      take: paginationDto?.pageSize,
      skip:
        paginationDto?.pageSize && paginationDto?.page
          ? (paginationDto?.page - 1) * paginationDto?.pageSize
          : undefined,
      orderBy: {
        [sortingDto?.sortBy || 'createdAt']: sortingDto?.sortOrder || 'desc',
      },
    };

    const total_transactions = await this.prisma.events.count();

    const transactions = await this.prisma.transactions.findMany({
      include: {
        user: {
          select: {
            id: true,
            name: true,
            firstname: true,
            UserType: {
              select: {
                name: true,
              },
            },
          },
        },
      },
     ...meta,
    });

    return {
      data: transactions,
      meta:{
        total: total_transactions,
        page: paginationDto?.page,
        pageSize: paginationDto?.pageSize,
        totalPages: paginationDto?.pageSize
          ? Math.ceil(total_transactions / paginationDto?.pageSize)
          : undefined,
      }
    }
  }

  async findOne(id: UUID) {
    const transaction = await this.prisma.transactions.findUnique({
      where: { id },
      include: {
        subscription: true,
        user: {
          select: {
            id: true,
            name: true,
            firstname: true,
            UserType: {
              select: {
                name: true,
              },
            },
          },
        },
      },
    });
    if (!transaction) {
      throw new NotFoundException('Transaction with this id does not exist');
    }

    return transaction;
  }

  async findByAggregatorRefAndAggregatorId(
    aggregatorReference: string,
    aggregatorId: string,
  ) {
    const transaction = await this.prisma.transactions.findUnique({
      where: {
        aggregatorId_aggregatorReference: { aggregatorId, aggregatorReference },
      },
      include: {
        user: {
          select: {
            id: true,
            name: true,
            firstname: true,
            UserType: {
              select: {
                name: true,
              },
            },
          },
        },
      },
    });

    if (!transaction) {
      throw new NotFoundException(
        'Transaction with this reference does not exist',
      );
    }

    return transaction;
  }

  async update(id: UUID, updateTransactionDto: UpdateTransactionDto) {
    if (Object.keys(updateTransactionDto).length === 0) {
      throw new HttpException(
        'At least one attribute is required',
        HttpStatus.BAD_REQUEST,
      );
    }

    const transaction = await this.findOne(id);

    if (!transaction) {
      throw new NotFoundException('Transaction with this id does not exist');
    }

    const updatedTransaction = await this.prisma.transactions.update({
      where: { id },
      data: {
        ...updateTransactionDto,
        extra: updateTransactionDto.extra
          ? updateTransactionDto.extra
          : undefined,
      },
      include: {
        user: {
          select: {
            id: true,
            name: true,
            firstname: true,
            UserType: {
              select: {
                name: true,
              },
            },
          },
        },
      },
    });

    if (!updatedTransaction) {
      throw new NotFoundException('Transaction with this id does not exist');
    }

    return updatedTransaction;
  }

  async remove(id: UUID) {
    const findTransaction = await this.findOne(id);

    if (!findTransaction) {
      throw new NotFoundException('Transaction with this id does not exist');
    }

    return this.prisma.transactions.update({
      where: { id },
      data: {
        deletedAt: new Date(),
      },
    });
  }
}
