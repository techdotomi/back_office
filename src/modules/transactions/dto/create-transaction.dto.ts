import { ApiProperty } from '@nestjs/swagger';
import { Status, Transactions } from '@prisma/client';
import { Decimal, JsonValue } from '@prisma/client/runtime/library';
import { Transform } from 'class-transformer';
import {
  IsDate,
  IsEnum,
  IsJSON,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  IsUUID,
  Validate,
} from 'class-validator';
import { AggregatorIdExistsRule } from 'src/modules/aggregator/dto/validation-rules/aggregator-id-exists.rule';

export class CreateTransactionDto
  implements
    Omit<
      Transactions,
      'id' | 'userId' | 'deletedAt' | 'createdAt' | 'updatedAt'
    >
{
  @IsNotEmpty()
  @IsUUID()
  @Validate(AggregatorIdExistsRule)
  @ApiProperty({
    name: 'aggregatorId',
    description: 'The id of the aggregator',
  })
  aggregatorId: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    name: 'aggregatorReference',
    description: 'The reference of the aggregator',
  })
  aggregatorReference: string;

  @IsNotEmpty()
  @IsNumber({maxDecimalPlaces: 2})
  @ApiProperty({
    name: 'price',
    description: "The price of the transaction, it's must be a string value",
  })
  price: Decimal;

  @IsNotEmpty()
  @IsEnum(Status)
  @Transform(({ value }) => value.toUpperCase() as Status)
  @ApiProperty({
    name: 'status',
    description: 'The status of the transaction',
  })
  status: Status;

  @IsOptional()
  @Transform(({ value }) => JSON.stringify(value))
  @IsJSON()
  @ApiProperty({
    name: 'extra',
    description: 'The extra of the transaction',
  })
  extra: JsonValue;

  @IsNotEmpty()
  @Transform(({ value }) => new Date(value))
  @IsDate()
  @ApiProperty({
    name: 'date',
    description: 'The date of the transaction',
  })
  date: Date;
}
