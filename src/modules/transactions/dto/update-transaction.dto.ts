import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateTransactionDto } from './create-transaction.dto';

import { Status } from '@prisma/client';
import { Decimal, JsonValue } from '@prisma/client/runtime/library';
import { Transform } from 'class-transformer';
import {
  IsDate,
  IsEnum,
  IsJSON,
  IsNumber,
  IsOptional,
  IsString,
  IsUUID,
  Validate,
} from 'class-validator';
import { AggregatorIdExistsRule } from 'src/modules/aggregator/dto/validation-rules/aggregator-id-exists.rule';

export class UpdateTransactionDto extends PartialType(CreateTransactionDto) {
  @IsOptional()
  @IsUUID()
  @Validate(AggregatorIdExistsRule)
  @ApiProperty({
    name: 'aggregatorId',
    description: 'The id of the aggregator',
  })
  aggregatorId: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    name: 'aggregatorReference',
    description: 'The reference of the aggregator',
  })
  aggregatorReference: string;

  @IsOptional()
  @IsNumber({maxDecimalPlaces: 2})
  @ApiProperty({
    name: 'price',
    description: 'The price of the transaction, it\'s must be a string value',
  })
  price: Decimal;

  @IsOptional()
  @IsEnum(Status)
  @Transform(({ value }) => value.toUpperCase() as Status)
  @ApiProperty({
    name: 'status',
    description: 'The status of the transaction',
  })
  status: Status;

  @IsOptional()
  @IsJSON()
  @ApiProperty({
    name: 'extra',
    description: 'The extra of the transaction',
  })
  extra: JsonValue;

  @IsOptional()
  @Transform(({ value }) => new Date(value))
  @IsDate()
  @ApiProperty({
    name: 'date',
    description: 'The date of the transaction',
  })
  date: Date;
}
