import { Injectable } from '@nestjs/common';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { UUID } from 'crypto';
import { TransactionsService } from '../../transactions.service';
import { Status } from '@prisma/client';

@ValidatorConstraint({ async: true })
@Injectable()
export class TransactionIsCompletedOrUsedRule
  implements ValidatorConstraintInterface
{
  constructor(private readonly TransactionService: TransactionsService) {}

  validate(transaction_id: string) {
    if (!transaction_id) return false;
    return this.TransactionService.findOne(transaction_id as UUID)
      .then((transaction) => {
        if (!transaction) return false;
        else
          return (
            transaction.status === Status.COMPLETED && !transaction.subscription
          );
      })
      .catch((error) => {
        console.error('Something is Wrong', error);
        return false;
      });
  }

  defaultMessage(): string {
    return "Transaction with this id does not exist or isn't completed or is used for another subscription";
  }
}
