import { IsIn, IsOptional, IsString } from 'class-validator';
import { SortingDto } from 'src/common/dto/pagination.dto';

export class SortingTransactionsDto extends SortingDto {
  @IsOptional()
  @IsString()
  @IsIn([
    'aggregatorId',
    'aggregatorReference',
    'price',
    'status',
    'extra',
    'userId',
    'date',
    'deletedAt',
    'createdAt',
    'updatedAt',
  ])
  sortBy?: string = 'createdAt';
}
