import { ApiProperty } from "@nestjs/swagger";
import { Prisma, Status, Transactions } from "@prisma/client";
import { Decimal } from "@prisma/client/runtime/library";

export class Transaction implements Transactions{
    @ApiProperty()
    id: string;
   
    @ApiProperty()
    aggregatorId: string;
    
    @ApiProperty()
    aggregatorReference: string;
    
    @ApiProperty()
    price: Decimal;
    
    @ApiProperty()
    status: Status;
    
    @ApiProperty()
    extra: Prisma.JsonValue | null;
    
    @ApiProperty()
    userId: string;
    
    @ApiProperty()
    date: Date;
   
    @ApiProperty()
    deletedAt: Date | null;
    
    @ApiProperty()
    createdAt: Date;
    
    @ApiProperty()
    updatedAt: Date;
}
