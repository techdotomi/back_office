import { Injectable, Logger } from '@nestjs/common';
import { CreateNotificationDto } from './dto/create-notification.dto';
import { UUID } from 'crypto';
import { filter, map, Subject } from 'rxjs';
import { Notification } from './entities/notification.entity';
import { PrismaService } from '../../libs/prisma/prisma.service';

@Injectable()
export class NotificationsService {
  constructor(private readonly prisma: PrismaService) {}
  private readonly logger = new Logger(NotificationsService.name);
  private readonly notificationSubject = new Subject<Notification>();

  async create(createNotificationDto: CreateNotificationDto) {
    const notification = await this.prisma.notifications.create({
      data: createNotificationDto,
    });
    this.logger.log(
      `Notification: ${notification.id} created successfully`,
      notification,
    );
    this.sendNotification(notification);
    return notification;
  }

  getUnreadNotifications(userId: UUID) {
    return this.prisma.notifications.findMany({
      where: {
        userId,
        isRead: false,
      },
      include: {
        User: {
          select: {
            id: true,
            name: true,
            firstname: true,
            nickname: true,
            email: true,
            phoneNumber: true,
            login: true,
            UserType: {
              select: {
                name: true,
              },
            },
          },
        },
      },
    });
  }

  sendNotification(notification: Notification) {
    this.logger.log(`Sending notification to user ${notification.userId}`);
    this.notificationSubject.next(notification);

    this.logger.log(
      `Notification ${notification.id} is ${this.notificationSubject.observed ? '' : 'not '}sent`,
    );
  }

  getNotificationStream(userId: UUID) {
    this.logger.log(`User ${userId} is subscribing to notifications`);
    return this.notificationSubject.pipe(
      filter(
        (notification) =>
          notification.userId === userId &&
          notification.active &&
          !notification.isRead,
      ),
      map((notification) => {
        this.logger.log(
          `User ${userId} received notification ${notification.id}`,
        );
        return notification;
      }),
    );
  }
}
