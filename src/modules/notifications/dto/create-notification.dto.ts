import { Notification } from '../entities/notification.entity';
export class CreateNotificationDto
  implements
    Omit<
      Notification,
      'id' | 'isRead' | 'readAt' | 'deletedAt' | 'createdAt' | 'updatedAt'
    >
{
  data: Record<string, any>;
  active: boolean;
  userId: string;
}
