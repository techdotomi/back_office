import {
  Controller,
  Param,
  Sse,
  UseGuards,
  Req,
  UnauthorizedException,
  Post,
  Body,
} from '@nestjs/common';
import { NotificationsService } from './notifications.service';
import { map, Observable } from 'rxjs';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { User } from '../users/entities/user.entity';
import { Request } from 'express';
import { UUID } from 'crypto';

@Controller('notifications')
export class NotificationsController {
  constructor(private readonly notificationsService: NotificationsService) {}

  @UseGuards(JwtAuthGuard)
  @Sse('sse/:userId/')
  sse(
    @Param('userId') userId: string = 'e3758a53-2382-44b5-a223-1eb93dc32d97',
    @Req() req: Request,
  ): Observable<MessageEvent> {
    if (userId !== (req.user as User).id) {
      throw new UnauthorizedException(
        'You are not authorized to access this resource',
      );
    }

    console.log('Hey !!!');

    this.notificationsService.create({
      userId: userId as UUID,
      active: true,
      data: {
        message: 'Hello from the server',
      },
    });

    return this.notificationsService.getNotificationStream(userId as UUID).pipe(
      map((notification) => {
        console.log(notification);
        return {
          data: {
            id: notification.id,
            data: notification.data,
            isRead: notification.isRead,
            active: notification.active,
          },
        } as MessageEvent;
      }),
    );
  }
  @Post('notify/:userId')
  async sendTestNotification(
    @Param('userId') userId: string,
    @Body('message') message: string,
  ) {
    await this.notificationsService.create({
      userId: userId as UUID,
      active: true,
      data: {
        message,
      },
    });
    return { success: true };
  }
  //
  // @Post()
  // create(@Body() createNotificationDto: CreateNotificationDto) {
  //   return this.notificationsService.create(createNotificationDto);
  // }
  //
  // @Get()
  // findAll() {
  //   return this.notificationsService.findAll();
  // }
  //
  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.notificationsService.findOne(+id);
  // }
  //
  // @Patch(':id')
  // update(
  //   @Param('id') id: string,
  //   @Body() updateNotificationDto: UpdateNotificationDto,
  // ) {
  //   return this.notificationsService.update(+id, updateNotificationDto);
  // }
  //
  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.notificationsService.remove(+id);
  // }
}
