import { Notifications } from '@prisma/client';
import { ApiProperty } from '@nestjs/swagger';
import { JsonValue } from '@prisma/client/runtime/library';

export class Notification implements Notifications {
  @ApiProperty()
  id: string;

  @ApiProperty()
  data: JsonValue;

  @ApiProperty()
  active: boolean;

  @ApiProperty()
  isRead: boolean;

  @ApiProperty()
  userId: string;

  @ApiProperty()
  readAt: Date | null;

  @ApiProperty()
  deletedAt: Date | null;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
