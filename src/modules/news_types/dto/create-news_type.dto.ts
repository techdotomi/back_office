import { ApiProperty } from '@nestjs/swagger';
import { NewsTypes } from '@prisma/client';
import { Transform } from 'class-transformer';
import { IsBoolean, IsNotEmpty, IsString } from 'class-validator';

export class CreateNewsTypeDto
  implements Omit<NewsTypes, 'id' | 'deletedAt' | 'createdAt' | 'updatedAt'>
{
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: 'The name of the userType',
    example: true,
  })
  name: string;

  @IsBoolean()
  @IsNotEmpty()
  @Transform(({ value }) => value.toString() === 'true')
  @ApiProperty({
    description: 'The active status of the userType',
    example: true,
  })
  active: boolean;
}
