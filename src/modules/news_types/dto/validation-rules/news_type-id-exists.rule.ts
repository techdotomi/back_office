import { Injectable, Logger } from '@nestjs/common';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { UUID } from 'crypto';
import { NewsTypesService } from 'src/modules/news_types/news_types.service';

@ValidatorConstraint({ async: true })
@Injectable()
export class NewsTypeIdExistsRule implements ValidatorConstraintInterface {
  constructor(private readonly NewsTypesService: NewsTypesService) {}
  private readonly logger = new Logger(NewsTypeIdExistsRule.name);

  async validate(news_type_id: string) {
    try {
      const news_type = await this.NewsTypesService.findOne(
        news_type_id as UUID,
      );
      return news_type !== null && news_type !== undefined;
    } catch (error) {
      this.logger.error('Something is Wrong', error);
      return false;
    }
  }

  defaultMessage(): string {
    return 'NewsType with this id does not exist';
  }
}
