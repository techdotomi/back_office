import { ApiProperty } from '@nestjs/swagger';
import { NewsTypes } from '@prisma/client';

export class NewsType implements NewsTypes {
  @ApiProperty()
  id: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  active: boolean;

  @ApiProperty()
  deletedAt: Date | null;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
