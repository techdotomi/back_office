import { Module } from '@nestjs/common';
import { NewsTypesService } from './news_types.service';
import { NewsTypesController } from './news_types.controller';
import { NewsTypeIdExistsRule } from './dto/validation-rules/news_type-id-exists.rule';

@Module({
  controllers: [NewsTypesController],
  providers: [NewsTypesService, NewsTypeIdExistsRule],
})
export class NewsTypesModule {}
