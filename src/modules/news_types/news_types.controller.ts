import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  HttpStatus,
  HttpCode,
} from '@nestjs/common';
import { NewsTypesService } from './news_types.service';
import { CreateNewsTypeDto } from './dto/create-news_type.dto';
import { UpdateNewsTypeDto } from './dto/update-news_type.dto';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { UUID } from 'crypto';

@Controller('news-types')
@ApiTags('news-types')
export class NewsTypesController {
  constructor(private readonly newsTypesService: NewsTypesService) {}

  @Post()
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  create(@Body() createNewsTypeDto: CreateNewsTypeDto) {
    return this.newsTypesService.create(createNewsTypeDto);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  findAll() {
    return this.newsTypesService.findAll();
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  findOne(@Param('id') id: string) {
    return this.newsTypesService.findOne(id as UUID);
  }

  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  update(
    @Param('id') id: string,
    @Body() updateNewsTypeDto: UpdateNewsTypeDto,
  ) {
    return this.newsTypesService.update(id as UUID, updateNewsTypeDto);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  remove(@Param('id') id: string) {
    return this.newsTypesService.remove(id as UUID);
  }
}
