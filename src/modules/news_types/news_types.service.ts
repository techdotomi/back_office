import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateNewsTypeDto } from './dto/create-news_type.dto';
import { UpdateNewsTypeDto } from './dto/update-news_type.dto';
import { PrismaService } from 'src/libs/prisma/prisma.service';
import { UUID } from 'crypto';

@Injectable()
export class NewsTypesService {
  constructor(private readonly prisma: PrismaService) {}

  async create(createNewsTypeDto: CreateNewsTypeDto) {
    return this.prisma.newsTypes.create({
      data: {
        ...createNewsTypeDto,
      },
    });
  }

  async findAll() {
    return this.prisma.newsTypes.findMany({
      orderBy: {  
        createdAt: 'desc',  
      },
    });
  }

  async findOne(id: UUID) {
    const news_type = await this.prisma.newsTypes.findUnique({
      where: {
        id: id,
      },
    });
    if (!news_type) {
      throw new NotFoundException('NewsType with this id does not exist');
    }
    return news_type;
  }

  async findByName(name: string) {
    const news_type = await this.prisma.newsTypes.findUnique({
      where: {
        name: name,
      },
    });
    if (!news_type) {
      throw new NotFoundException('NewsType with this name does not exist');
    }
    return news_type
  }

  async update(id: UUID, updateNewsTypeDto: UpdateNewsTypeDto) {
    if (Object.keys(updateNewsTypeDto).length === 0) {
      throw new BadRequestException('At least one attribute is required');
    }
    const findNewsType = await this.prisma.newsTypes.findUnique({
      where: { id },
    });

    if (!findNewsType) {
      throw new NotFoundException('NewsType not found');
    }

    const newsType = await this.prisma.newsTypes.update({
      where: {
        id: id,
      },
      data: {
        ...updateNewsTypeDto,
      },
    });

    return newsType;
  }

  async remove(id: UUID) {
    const newsType = await this.prisma.newsTypes.findUnique({
      where: { id },
    });

    if (!newsType) {
      throw new NotFoundException('NewsType not found');
    }

    await this.prisma.newsTypes.delete({
      where: {
        id,
      },
    });

    return 'Done';
  }
}
