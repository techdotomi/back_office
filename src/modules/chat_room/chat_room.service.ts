import { Injectable, NotFoundException, Logger } from '@nestjs/common';
import { CreateChatRoomDto } from './dto/create-chat_room.dto';
import { UUID } from 'crypto';
import { PrismaService } from 'src/libs/prisma/prisma.service';
import { MessageService } from '../message/message.service';
import { CreateMessageDto } from '../message/dto/create-message.dto';

@Injectable()
export class ChatRoomService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly messageService: MessageService,
  ) {}

  findAll(userId: UUID) {
    return this.prisma.chatRooms.findMany({
      where: {
        OR: [{ initiatorId: userId }, { participantId: userId }],
      },
      orderBy: {
        createdAt: 'desc',
      },
    });
  }

  findOneByParticipants(initiatorId: UUID, participantId: UUID) {
    return this.prisma.chatRooms.findFirst({
      where: {
        OR: [
          { initiatorId, participantId },
          { initiatorId: participantId, participantId: initiatorId },
        ],
      },
    });
  }

  create(createChatRoomDto: CreateChatRoomDto) {
    return this.prisma.chatRooms.create({
      data: {
        ...createChatRoomDto,
      },
      include: {
        participant: {
          include: {
            UserType: true,
          },
        },
        initiator: {
          include: {
            UserType: true,
          },
        },
      },
    });
  }
  async findOrCreateChatRoom(createChatRoomDto: CreateChatRoomDto) {
    const existingChatRoom = await this.prisma.chatRooms.findFirst({
      where: {
        OR: [
          {
            AND: [
              { participantId: createChatRoomDto.participantId },
              { initiatorId: createChatRoomDto.initiatorId },
            ],
          },
          {
            AND: [
              { participantId: createChatRoomDto.initiatorId },
              { initiatorId: createChatRoomDto.participantId },
            ],
          },
        ],
      },
      include: {
        participant: {
          select: {
            id: true,
            name: true,
            firstname: true,
            nickname: true,
            phoneNumber: true,
            sex: true,
            avatarPicture: true,
            email: true,
            online: true,
            UserType: true,
          },
        },
        initiator: {
          select: {
            id: true,
            name: true,
            firstname: true,
            nickname: true,
            phoneNumber: true,
            sex: true,
            avatarPicture: true,
            email: true,
            online: true,
            UserType: true,
          },
        },
      },
    });

    if (existingChatRoom) {
      return existingChatRoom;
    }

    return this.prisma.chatRooms.create({
      data: {
        ...createChatRoomDto,
      },
      include: {
        participant: {
          select: {
            id: true,
            name: true,
            firstname: true,
            nickname: true,
            phoneNumber: true,
            sex: true,
            avatarPicture: true,
            email: true,
            online: true,
            UserType: true,
          },
        },
        initiator: {
          select: {
            id: true,
            name: true,
            firstname: true,
            nickname: true,
            phoneNumber: true,
            sex: true,
            avatarPicture: true,
            email: true,
            online: true,
            UserType: true,
          },
        },
      },
    });
  }

  async findOne(id: UUID) {
    const chatRoom = await this.prisma.chatRooms.findUnique({
      where: {
        id,
      },
      include: {
        participant: {
          select: {
            id: true,
            name: true,
            firstname: true,
            nickname: true,
            phoneNumber: true,
            sex: true,
            avatarPicture: true,
            email: true,
            online: true,
            UserType: true,
          },
        },
        initiator: {
          select: {
            id: true,
            name: true,
            firstname: true,
            nickname: true,
            phoneNumber: true,
            sex: true,
            avatarPicture: true,
            email: true,
            online: true,
            UserType: true,
          },
        },
      },
    });

    if (!chatRoom) {
      throw new NotFoundException(`ChatRoom with ID ${id} not found`);
    }

    return chatRoom;
  }

  async chatRoomByUser(userId: UUID) {
    const chatRooms = await this.prisma.chatRooms.findMany({
      where: {
        OR: [{ initiatorId: userId }, { participantId: userId }],
      },
      include: {
        participant: {
          select: {
            id: true,
            name: true,
            firstname: true,
            nickname: true,
            phoneNumber: true,
            sex: true,
            avatarPicture: true,
            email: true,
            online: true,
            UserType: true,
          },
        },
        initiator: {
          select: {
            id: true,
            name: true,
            firstname: true,
            nickname: true,
            phoneNumber: true,
            sex: true,
            avatarPicture: true,
            email: true,
            online: true,
            UserType: true,
          },
        },
      },
    });

    if (!chatRooms) {
      throw new NotFoundException(
        `ChatRooms with participantId or initiatorId ${userId} not found`,
      );
    }

    return chatRooms;
  }

  async addMessage(createMessageDto: CreateMessageDto) {
    return this.messageService.create(createMessageDto);
  }

  userConnected(userId: UUID) {
    return this.prisma.users.update({
      where: {
        id: userId,
      },
      data: {
        online: true,
      },
    });
  }

  userDisconnected(userId: UUID) {
    return this.prisma.users.update({
      where: {
        id: userId,
      },
      data: {
        online: false,
      },
    });
  }

  // remove(id: UUID) {
  //   return `This action removes a #${id} chatRoom`;
  // }
}
