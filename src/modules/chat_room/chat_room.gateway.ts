import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { ChatRoomService } from './chat_room.service';
import { CreateChatRoomDto } from './dto/create-chat_room.dto';
import {
  Body,
  Logger,
  UnauthorizedException,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { Server, Socket } from 'socket.io';
import { UsersService } from '../users/users.service';
import { UUID } from 'crypto';
import { UpdateUserDto } from '../users/dto/update-user.dto';
import { MessageService } from '../message/message.service';
import { CreateMessageDto } from '../message/dto/create-message.dto';
import { JoinChatRoomDto } from './dto/join-chat_room.dto';
import { NotificationsService } from '../notifications/notifications.service';

// import { FileInterceptor } from '@nestjs/platform-express';

@WebSocketGateway({
  cors: {
    origin: '*',
    methods: ['GET', 'POST'],
    credentials: true,
  },
})

// @UsePipes(new ValidationPipe())
export class ChatRoomGateway
  implements OnGatewayConnection, OnGatewayDisconnect
{
  private readonly logger = new Logger(ChatRoomGateway.name);

  @WebSocketServer() server: Server;
  constructor(
    private readonly chatRoomService: ChatRoomService,
    private readonly userService: UsersService,
    private readonly messageService: MessageService,
    // private readonly notificationsService: NotificationsService,
  ) {}

  async handleConnection(@ConnectedSocket() client: Socket) {
    try {
      const userId = client.handshake.query.userId as string;
      if (!userId) {
        client.disconnect(true);
        return;
      }

      const user = await this.userService.update(
        userId as UUID,
        { online: true } as UpdateUserDto,
      );
      this.logger.log(`Client connected: ${client.id} - User: ${user.id}`);
    } catch (error) {
      this.handleConnectionError(client, error);
    }
  }

  async handleDisconnect(@ConnectedSocket() client: Socket) {
    try {
      const userId = client.handshake.query.userId as string;

      if (!userId) {
        return;
      }

      const user = await this.userService.update(
        userId as UUID,
        { online: false } as UpdateUserDto,
      );
      this.logger.log(`Client disconnected: ${client.id} - User: ${user.id}`);
    } catch (error) {
      this.handleConnectionError(client, error);
    }
  }

  @SubscribeMessage('createChatRoom')
  async handleCreateChatRoom(
    @ConnectedSocket() client: Socket,
    @Body() createChatRoomDto: CreateChatRoomDto,
  ) {
    try {
      console.log('createChatRoomDto:', createChatRoomDto);

      const chatRoomExists = await this.chatRoomService.findOneByParticipants(createChatRoomDto.initiatorId as UUID, createChatRoomDto.participantId as UUID);
      if (chatRoomExists) {
        client.join(chatRoomExists.id);
        return this.server.to(chatRoomExists.id).emit('chatRoomJoined', chatRoomExists);
      }

      // const chatRoom = await this.chatRoomService.create(createChatRoomDto);
      // client.join(chatRoom.id);
      // await this.notificationsService.create({
      //   userId: chatRoom.initiatorId,
      //   active: true,
      //   data: {
      //     title: 'Chat room created',
      //     message: `Chat room with ${chatRoom.participantId} has been created`,
      //     type: 'chat_room',
      //     reference: chatRoom.id,
      //   },
      // });
      const chatRoom = await this.chatRoomService.findOrCreateChatRoom(createChatRoomDto);
      await client.join(chatRoom.id);
      // await this.notificationsService.create({
      //   userId: chatRoom.initiatorId,
      //   active: true,
      //   data: {
      //     title: 'Chat room created',
      //     message: `Chat room with ${chatRoom.participantId} has been created`,
      //     type: 'chat_room',
      //     reference: chatRoom.id,
      //   },
      // });
      this.server.to(chatRoom.id).emit('chatRoomCreated', chatRoom);
    } catch (error) {
      this.handleConnectionError(client, error);
    }
  }

  @SubscribeMessage('joinChatRoom')
  async handleJoinChatRoom(
    @ConnectedSocket() client: Socket,
    @Body() joinChatRoomDto: JoinChatRoomDto,
  ) {
    try {
      const chatRoom = await this.chatRoomService.findOne(
        joinChatRoomDto.chatRoomId as UUID,
      );
      if (
        chatRoom.initiatorId !== joinChatRoomDto.userId &&
        chatRoom.participantId !== joinChatRoomDto.userId
      ) {
        throw new UnauthorizedException(
          'User is not allowed to join this chat room',
        );
      }
      await client.join(chatRoom.id);
      this.server.to(chatRoom.id).emit('chatRoomJoined', chatRoom.id);
      this.logger.log(
        `Client : ${joinChatRoomDto.userId} - Join chatRoom: ${chatRoom.id}`,
      );
    } catch (error) {
      this.handleConnectionError(client, error);
    }
  }

  // @UseInterceptors(FileInterceptor('mediaUrl'))
  @SubscribeMessage('sendMessage')
  async handleSendMessage(
    @ConnectedSocket() client: Socket,
    @MessageBody() createMessageDto: CreateMessageDto,
    // @UploadedFile(
    //   new ParseFilePipeBuilder()
    //     .addFileTypeValidator({
    //       fileType: 'jpeg|jpg|png|webp',
    //     })
    //     .addMaxSizeValidator({
    //       maxSize: 100_000, // 100KB
    //     })
    //     .build({
    //       fileIsRequired: false,
    //     }),
    // )
    // mediaUrl?: Express.Multer.File,
  ) {
    try {
      const chatRoom = await this.chatRoomService.findOne(
        createMessageDto.chatRoomId as UUID,
      );
      if (
        chatRoom.initiatorId !== createMessageDto.senderId &&
        chatRoom.participantId !== createMessageDto.senderId
      ) {
        throw new UnauthorizedException(
          'User is not allowed to join this chat room',
        );
      }
      // if (mediaUrl) {
      //   console.log(mediaUrl.path)
      // }
      const message = await this.messageService.create(createMessageDto);

      // console.log(message);
      this.server.to(createMessageDto.chatRoomId).emit('messageSent', message);
    } catch (error) {
      this.handleConnectionError(client, error);
    }
  }

  private handleConnectionError(socket: Socket, error: Error): void {
    this.logger.error(
      `Connection error for socket ${socket.id}: ${error.message}`,
    );
    socket.emit('exception', 'Authentication error');
    socket.disconnect();
  }
}
