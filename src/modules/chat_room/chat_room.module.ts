import { Module } from '@nestjs/common';
import { ChatRoomService } from './chat_room.service';
import { ChatRoomGateway } from './chat_room.gateway';
import { UsersModule } from '../users/users.module';
import { MessageModule } from '../message/message.module';
import { ChatRoomIdExistsRule } from './dto/validation-rules/chat_room-id-exists';
import { CreateMessageDto } from '../message/dto/create-message.dto';
import { UserIsActiveRule } from '../users/dto/validation-rules/user-is-active.rule';
import { UserIdExistsRule } from '../users/dto/validation-rules/user-id-exists.rule';
import { NotificationsService } from '../notifications/notifications.service';
import { NotificationsModule } from '../notifications/notifications.module';

@Module({
  providers: [
    ChatRoomGateway,
    ChatRoomService,
    ChatRoomIdExistsRule,
    CreateMessageDto,
    UserIsActiveRule,
    UserIdExistsRule,
    // NotificationsService,
  ],
  imports: [UsersModule, MessageModule ],
  exports: [ChatRoomService],
})
export class ChatRoomModule {}
// NotificationsModule