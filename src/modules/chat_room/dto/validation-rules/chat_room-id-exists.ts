import { Injectable, Logger } from '@nestjs/common';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { UUID } from 'crypto';
import { NotFoundError } from 'rxjs';
import { ChatRoomService } from 'src/modules/chat_room/chat_room.service';

@ValidatorConstraint({ async: true })
@Injectable()
export class ChatRoomIdExistsRule implements ValidatorConstraintInterface {
  constructor(private readonly ChatRoomService: ChatRoomService) {}
  private readonly logger = new Logger(ChatRoomIdExistsRule.name);

  validate(chat_room_id: string) {
    if (!chat_room_id) return false;
    return this.ChatRoomService.findOne(chat_room_id as UUID)
      .then((chat_room) => {
        return !(chat_room instanceof NotFoundError);
      })
      .catch((error) => {
        this.logger.error('Something is Wrong', error);
        return false;
      });
  }

  defaultMessage(): string {
    return 'ChatRoom with this id does not exist';
  }
}
