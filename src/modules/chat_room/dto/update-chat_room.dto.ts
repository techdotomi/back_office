import { PartialType } from '@nestjs/mapped-types';
import { CreateChatRoomDto } from './create-chat_room.dto';
import { IsOptional, IsUUID, Validate } from 'class-validator';
import { UserIsActiveRule } from 'src/modules/users/dto/validation-rules/user-is-active.rule';

export class UpdateChatRoomDto extends PartialType(CreateChatRoomDto) {
  @IsOptional()
  @IsUUID()
  // @Validate(UserIsActiveRule)
  initiatorId: string;

  @IsOptional()
  @IsUUID()
  // @Validate(UserIsActiveRule)
  participantId: string;
}
