import { ChatRooms } from '@prisma/client';
import { IsNotEmpty, IsUUID, Validate } from 'class-validator';
import { UserIsActiveRule } from 'src/modules/users/dto/validation-rules/user-is-active.rule';

export class CreateChatRoomDto
  implements Omit<ChatRooms, 'id' | 'createdAt' | 'updatedAt'>
{
  @IsNotEmpty()
  @IsUUID()
  // @Validate(UserIsActiveRule)
  initiatorId: string;

  @IsNotEmpty()
  @IsUUID()
  // @Validate(UserIsActiveRule)
  participantId: string;
}
