import { IsNotEmpty, IsUUID, Validate } from 'class-validator';
import { UserIsActiveRule } from 'src/modules/users/dto/validation-rules/user-is-active.rule';
import { ChatRoomIdExistsRule } from './validation-rules/chat_room-id-exists';

export class JoinChatRoomDto {
  @IsNotEmpty()
  @IsUUID()
  @Validate(ChatRoomIdExistsRule)
  chatRoomId: string;

  @IsNotEmpty()
  @IsUUID()
  // @Validate(UserIsActiveRule)
  userId: string;
}
