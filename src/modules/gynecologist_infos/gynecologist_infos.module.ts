import { Module } from '@nestjs/common';
import { GynecologistInfosService } from './gynecologist_infos.service';
import { PrismaModule } from 'src/libs/prisma/prisma.module';
import { PrismaClient } from "@prisma/client";

@Module({
  providers: [GynecologistInfosService],
  imports: [PrismaModule, PrismaClient],
  exports: [GynecologistInfosService]
})
export class GynecologistInfosModule {}
