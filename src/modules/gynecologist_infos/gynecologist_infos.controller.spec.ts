import { Test, TestingModule } from '@nestjs/testing';
import { GynecologistInfosController } from './gynecologist_infos.controller';
import { GynecologistInfosService } from './gynecologist_infos.service';

describe('GynecologistInfosController', () => {
  let controller: GynecologistInfosController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [GynecologistInfosController],
      providers: [GynecologistInfosService],
    }).compile();

    controller = module.get<GynecologistInfosController>(GynecologistInfosController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
