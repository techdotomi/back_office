import { Injectable } from '@nestjs/common';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { UUID } from 'crypto';
import { NotFoundError } from 'rxjs';
import { GynecologistInfosService } from 'src/modules/gynecologist_infos/gynecologist_infos.service';

@ValidatorConstraint({ async: true })
@Injectable()
export class GynecologistInfoIsActiveRule implements ValidatorConstraintInterface {
  constructor(private readonly GynecologistInfoService: GynecologistInfosService) {}

  validate(gynecologist_info_id: string) {
    if (!gynecologist_info_id) return false;
    return this.GynecologistInfoService.findOne(gynecologist_info_id as UUID)
      .then((gynecologist_info) => {
        if (gynecologist_info instanceof NotFoundError) {
          return false;
        }
        return gynecologist_info.active;
      })
      .catch((error) => {
        console.error('Something is Wrong', error);
        return false;
      });
  }

  defaultMessage(): string {
    return 'GynecologistInfo with this id does not exist';
  }
}
