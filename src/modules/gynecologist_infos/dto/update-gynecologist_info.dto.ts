import { PartialType } from "@nestjs/mapped-types";
import {
  IsBoolean,
  IsEmail,
  IsOptional,
  IsPhoneNumber,
  IsString,
} from "class-validator";
import { Transform } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";
import { CreateGynecologistInfoDto } from "./create-gynecologist_info.dto";

export class UpdateGynecologistInfoDto extends PartialType(
  CreateGynecologistInfoDto,
) {
  @IsString()
  @IsOptional()
  @ApiProperty({
    name: "education",
  })
  education: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    name: "grade",
  })
  grade: string;

  @IsString()
  @IsPhoneNumber()
  @ApiProperty({
    name: "officePhoneNumber",
  })
  officePhoneNumber: string;

  @IsString()
  @IsEmail()
  @IsOptional()
  @ApiProperty({
    name: "officeEmail",
  })
  officeEmail: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    name: "officeAddress",
  })
  officeAddress: string;

  @Transform(({ value }) => value.toString() === "true")
  @IsBoolean()
  @ApiProperty({
    name: "active",
  })
  active: boolean;
}
