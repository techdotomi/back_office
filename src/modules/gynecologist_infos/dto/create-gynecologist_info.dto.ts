import {
  IsBoolean,
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsPhoneNumber,
  IsString,
} from "class-validator";
import { Transform } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";
import { GynecologistInfos } from "@prisma/client";
export class CreateGynecologistInfoDto
  implements
    Omit<
      GynecologistInfos,
      "id" | "userId" | "deletedAt" | "createdAt" | "updatedAt"
    >
{
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    name: "education",
  })
  education: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    name: "grade",
  })
  grade: string;

  @IsString()
  @IsPhoneNumber()
  @ApiProperty({
    name: "officePhoneNumber",
  })
  officePhoneNumber: string;

  @IsString()
  @IsEmail()
  @IsOptional()
  @ApiProperty({
    name: "officeEmail",
  })
  officeEmail: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    name: "officeAddress",
  })
  officeAddress: string;

  @Transform(({ value }) => value.toString() === "true")
  @IsBoolean()
  @ApiProperty({
    name: "active",
  })
  active: boolean;

  //   @IsUUID()
  //   @IsNotEmpty()
  //   @ApiProperty({
  //     name:"userId"
  //   })
  //   userId: string;
}
