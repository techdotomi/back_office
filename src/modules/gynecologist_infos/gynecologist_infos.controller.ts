import { Controller, Get, Param, Delete } from "@nestjs/common";
import { GynecologistInfosService } from "./gynecologist_infos.service";
import { UUID } from "crypto";

@Controller("gynecologist-infos")
export class GynecologistInfosController {
  constructor(
    private readonly gynecologistInfosService: GynecologistInfosService,
  ) {}

  @Get()
  findAll() {
    return this.gynecologistInfosService.findAll();
  }

  @Get(":id")
  findOne(@Param("id") id: string) {
    return this.gynecologistInfosService.findOne(id as UUID);
  }

  @Delete(":id")
  remove(@Param("id") id: string) {
    return this.gynecologistInfosService.remove(id as UUID);
  }
}
