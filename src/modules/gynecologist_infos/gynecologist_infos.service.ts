import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateGynecologistInfoDto } from './dto/create-gynecologist_info.dto';
import { UpdateGynecologistInfoDto } from './dto/update-gynecologist_info.dto';
import { PrismaService } from 'src/libs/prisma/prisma.service';
import { Users } from '@prisma/client';
import { UUID } from 'crypto';

@Injectable()
export class GynecologistInfosService {
  constructor(private readonly prisma: PrismaService) {}
  async create(
    createGynecologistInfoDto: CreateGynecologistInfoDto,
    user: Users,
  ) {
    return this.prisma.gynecologistInfos.create({
      data: {
        ...createGynecologistInfoDto,
        user: {
          connect: {
            id: user.id,
          },
        },
      },
    });
  }

  findAll() {
    return this.prisma.gynecologistInfos.findMany({
      include: {
        user: true,
      },
    });
  }

  async findOne(id: UUID) {
    const gyneco = await this.prisma.gynecologistInfos.findUnique({
      where: {
        id,
      },
      include: {
        user: true,
      },
    });

    if (!gyneco) {
      throw new NotFoundException(`GynecologistInfo with ID ${id} not found`);
    }

    return gyneco;
  }

  update(id: UUID, updateGynecologistInfoDto: UpdateGynecologistInfoDto) {
    return this.prisma.gynecologistInfos.update({
      where: {
        id,
      },
      data: {
        ...updateGynecologistInfoDto,
      },
    });
  }

  async remove(id: UUID) {
    const gynecologistInfo = await this.prisma.gynecologistInfos.findUnique({
      where: {
        id,
      },
      include: {
        user: true,
      },
    });
    if (!gynecologistInfo) {
      throw new NotFoundException(`GynecologistInfo with ID ${id} not found`);
    }
    return this.prisma.gynecologistInfos.update({
      where: {
        id,
      },
      data: {
        active: false,
        deletedAt: new Date(),
      },
    });
  }
}
