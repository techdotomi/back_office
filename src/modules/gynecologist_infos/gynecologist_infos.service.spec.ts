import { Test, TestingModule } from '@nestjs/testing';
import { GynecologistInfosService } from './gynecologist_infos.service';

describe('GynecologistInfosService', () => {
  let service: GynecologistInfosService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [GynecologistInfosService],
    }).compile();

    service = module.get<GynecologistInfosService>(GynecologistInfosService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
