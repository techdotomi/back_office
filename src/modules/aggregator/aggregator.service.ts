import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateAggregatorDto } from './dto/create-aggregator.dto';
import { UpdateAggregatorDto } from './dto/update-aggregator.dto';
import { PrismaService } from 'src/libs/prisma/prisma.service';
import { UUID } from 'crypto';

@Injectable()
export class AggregatorService {
  constructor(private readonly prisma: PrismaService) {}

  async create(createAggregatorDto: CreateAggregatorDto) {
    return this.prisma.aggregators.create({
      data: createAggregatorDto,
    });
  }

  findAll() {
    return this.prisma.aggregators.findMany({
      orderBy: {  
        createdAt: 'desc',  
      },
    });
  }

  async findOne(id: UUID) {
    const aggregator = await this.prisma.aggregators.findUnique({
      where: {
        id,
      },
    });
    if (!aggregator) {
      throw new NotFoundException(`Aggregator with this id does not found`);
    }

    return aggregator;
  }

  async update(id: UUID, updateAggregatorDto: UpdateAggregatorDto) {
    if (Object.keys(updateAggregatorDto).length === 0) {
      throw new HttpException(
        'At least one field is required to update',
        HttpStatus.BAD_REQUEST,
      );
    }

    const aggregator = await this.prisma.aggregators.findUnique({
      where: {
        id,
      },
    });

    if (!aggregator) {
      throw new NotFoundException(`Aggregator with this id does not found`);
    }

    return this.prisma.aggregators.update({
      where: {
        id,
      },
      data: updateAggregatorDto,
    });
  }

  remove(id: UUID) {
    const aggregator = this.findOne(id);

    if (!aggregator) {
      throw new NotFoundException(`Aggregator with this id does not found`);
    }

    return this.prisma.aggregators.update({
      where: {
        id,
      },
      data: {
        active: false,
        deletedAt: new Date(),
      },
    });
  }
}
