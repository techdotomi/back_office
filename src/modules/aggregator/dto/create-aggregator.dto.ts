import { ApiProperty } from "@nestjs/swagger";
import { Aggregators } from "@prisma/client";
import { Transform } from "class-transformer";
import { IsBoolean, IsNotEmpty, IsOptional, IsString } from "class-validator";

export class CreateAggregatorDto 
implements Omit<Aggregators, 'id' | 'deletedAt' | 'createdAt' | 'updatedAt' | 'active'>{
    @IsNotEmpty()
    @IsString()
    @ApiProperty({
        name: 'name',
        description: 'The name of the payment aggregator',
    })
    name: string;

    @IsOptional()
    @Transform(({ value }) => (value.toString() === 'true' ? true : false))
    @IsBoolean()
    @ApiProperty({
        name: 'active',
        description: 'The status of the payment aggregator',
    })
    active: boolean = true;
}
