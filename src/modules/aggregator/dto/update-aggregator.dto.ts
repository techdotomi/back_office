import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateAggregatorDto } from './create-aggregator.dto';
import { IsBoolean, IsOptional, IsString } from 'class-validator';
import { Transform } from 'class-transformer';

export class UpdateAggregatorDto extends PartialType(CreateAggregatorDto) {
    @IsOptional()
    @IsString()
    @ApiProperty({
        name: 'name',
        description: 'The name of the payment aggregator',
    })
    name: string;

    @IsOptional()
    @Transform(({ value }) => (value.toString() === 'true' ? true : false))
    @IsBoolean()
    @ApiProperty({
        name: 'active',
        description: 'The status of the payment aggregator',
    })
    active: boolean;
}
