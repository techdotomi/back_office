import { Injectable } from '@nestjs/common';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { UUID } from 'crypto';
import { NotFoundError } from 'rxjs';
import { AggregatorService } from '../../aggregator.service';

@ValidatorConstraint({ async: true })
@Injectable()
export class AggregatorIdExistsRule implements ValidatorConstraintInterface {
  constructor(private readonly AggragatorService: AggregatorService) {}

  validate(aggregator_id: string) {
    if (!aggregator_id) return false;
    return this.AggragatorService.findOne(aggregator_id as UUID)
      .then((aggregator) => {
        return !(aggregator instanceof NotFoundError);
      })
      .catch((error) => {
        console.error('Something is Wrong', error);
        return false;
      });
  }

  defaultMessage(): string {
    return 'Aggragator with this id does not exist';
  }
}
