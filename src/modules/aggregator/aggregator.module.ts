import { Module } from '@nestjs/common';
import { AggregatorService } from './aggregator.service';
import { AggregatorController } from './aggregator.controller';
import { AggregatorIdExistsRule } from './dto/validation-rules/aggregator-id-exists.rule';

@Module({
  controllers: [AggregatorController],
  providers: [AggregatorService, AggregatorIdExistsRule],
})
export class AggregatorModule {}
