import { ApiProperty } from '@nestjs/swagger';
import { Aggregators } from '@prisma/client';

export class Aggregator implements Aggregators {
  @ApiProperty()
  id: string;
  
  @ApiProperty()
  name: string;
  
  @ApiProperty()
  active: boolean;
  
  @ApiProperty()
  deletedAt: Date | null;
  
  @ApiProperty()
  createdAt: Date;
  
  @ApiProperty()
  updatedAt: Date;
}

