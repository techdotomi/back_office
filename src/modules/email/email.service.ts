import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { ConfigService } from '@nestjs/config';
import { SendMailDto } from './dto/send-email.dto';

@Injectable()
export class EmailService {
  constructor(
    private readonly mailerService: MailerService,
    private readonly configService: ConfigService,
  ) {}

  async sendEmail(sendMailDto: SendMailDto) {
    const mailerOptions = {
      to: sendMailDto.to,
      subject: sendMailDto.subject,
      // text: `${sendMailDto.html}`, // plaintext body
      html: sendMailDto.html ? `<b>${sendMailDto.html}</b>` : undefined, // HTML body
      template: sendMailDto.template,
      context: sendMailDto.context,
    };

    await this.mailerService.sendMail(mailerOptions);
  }
}
