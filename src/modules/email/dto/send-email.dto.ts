import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class SendMailDto {
  @IsNotEmpty()
  @ApiProperty({
    name: 'to',
    description: 'The email address to send the email to',
  })
  @IsString()
  to: string;

  @IsNotEmpty()
  @ApiProperty({
    name: 'subject',
    description: 'The subject of the email',
  })
  @IsString()
  subject: string;

  @IsOptional()
  @ApiProperty({
    name: 'html',
    description: 'The HTML content of the email',
  })
  @IsString()
  html?: string;

  @IsOptional()
  @ApiProperty({
    name: 'template',
    description: 'The template to use for the email',
  })
  @IsString()
  template?: string;

  @IsOptional()
  @ApiProperty({
    name: 'context',
    description: 'The context to use for the email',
  })
  context?: Record<string, string | string[]> = {};


  attachments?: Array<Record<string, string>> = [];
}