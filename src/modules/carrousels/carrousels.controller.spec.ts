import { Test, TestingModule } from '@nestjs/testing';
import { CarrouselsController } from './carrousels.controller';
import { CarrouselsService } from './carrousels.service';

describe('CarrouselsController', () => {
  let controller: CarrouselsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CarrouselsController],
      providers: [CarrouselsService],
    }).compile();

    controller = module.get<CarrouselsController>(CarrouselsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
