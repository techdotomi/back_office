import { Module } from '@nestjs/common';
import { CarrouselsService } from './carrousels.service';
import { CarrouselsController } from './carrousels.controller';

import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { generateRandomString } from 'src/libs/utils';
import {extname} from 'path';
import { FileUploadModule } from '../file_upload/file_upload.module';

@Module({
  controllers: [CarrouselsController],
  providers: [CarrouselsService],
  imports: [FileUploadModule,
    MulterModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        storage: diskStorage({
          destination: configService.get<string>('UPLOAD_PATH'),
          filename: (req, file, cb) => {
            const filename: string = `${file.fieldname}-${Date.now()}-${generateRandomString(10)}${extname(file.originalname)}`;
            cb(null, filename);
          },
        }),
      }),
      inject: [ConfigService],
    }),
  ]
})
export class CarrouselsModule {}
