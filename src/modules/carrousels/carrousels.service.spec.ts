import { Test, TestingModule } from '@nestjs/testing';
import { CarrouselsService } from './carrousels.service';

describe('CarrouselsService', () => {
  let service: CarrouselsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CarrouselsService],
    }).compile();

    service = module.get<CarrouselsService>(CarrouselsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
