import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateCarrouselDto } from './dto/create-carrousel.dto';
import { UpdateCarrouselDto } from './dto/update-carrousel.dto';
import { PrismaService } from 'src/libs/prisma/prisma.service';
import { FileUploadService } from '../file_upload/file_upload.service';
import { UUID } from 'crypto';
import { PaginationDto } from 'src/common/dto/pagination.dto';
import { SortingEventsDto } from '../events/dto/sorting-event.dto';

@Injectable()
export class CarrouselsService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly fileUploadService: FileUploadService,
  ) {}

  async create(createCarrouselDto: CreateCarrouselDto, req: any) {
    return this.prisma.carrousels.create({
      data: {
        ...createCarrouselDto,
        authorId: req.user.id,
      },
      include: {
        author: {
          select: {
            id: true,
            name: true,
            firstname: true,
            UserType: {
              select: {
                name: true,
              },
            },
          },
        },
      },
    });
  }

  async findAll(paginationDto?: PaginationDto, sortingDto?: SortingEventsDto) {
    const meta = {
      take: paginationDto?.pageSize,
      skip:
        paginationDto?.pageSize && paginationDto?.page
          ? (paginationDto?.page - 1) * paginationDto?.pageSize
          : undefined,
      orderBy: {
        [sortingDto?.sortBy || 'createdAt']: sortingDto?.sortOrder || 'desc',
      },
    };

    const total_carrousels = await this.prisma.carrousels.count();

    const carrousels = await this.prisma.carrousels.findMany({
      include: {
        author: {
          select: {
            id: true,
            name: true,
            firstname: true,
            UserType: {
              select: {
                name: true,
              },
            },
          },
        },
      },
      ...meta,
    });

    return {
      data: carrousels,
      meta: {
        total: total_carrousels,
        page: paginationDto?.page,
        pageSize: paginationDto?.pageSize,
        totalPages: paginationDto?.pageSize
          ? Math.ceil(total_carrousels / paginationDto?.pageSize)
          : undefined,
      },
    };
  }

  async findOne(id: UUID) {
    const carrousel = await this.prisma.carrousels.findUnique({
      where: {
        id: id,
      },
      include: {
        author: {
          select: {
            id: true,
            name: true,
            firstname: true,
            UserType: {
              select: {
                name: true,
              },
            },
          },
        },
      },
    });
    if (!carrousel) {
      throw new NotFoundException('Carrousel with this id does not exist');
    }

    return carrousel;
  }

  async update(id: UUID, updateCarrouselDto: UpdateCarrouselDto) {
    if (Object.keys(updateCarrouselDto).length === 0) {
      throw new BadRequestException('At least one attribute is required');
    }

    const findCarrousel = await this.findOne(id);

    if ('id' in findCarrousel) {
      if (updateCarrouselDto.image && findCarrousel.image) {
        await this.fileUploadService.deleteFile(findCarrousel.image);
      }

      const carrousel = await this.prisma.carrousels.update({
        where: {
          id: id,
        },
        data: {
          ...updateCarrouselDto,
        },
        include: {
          author: {
            select: {
              id: true,
              name: true,
              firstname: true,
              UserType: {
                select: {
                  name: true,
                },
              },
            },
          },
        },
      });

      if (!carrousel) {
        throw new HttpException(
          'Carrousel with this id does not exist',
          HttpStatus.NOT_FOUND,
        );
      }

      return carrousel;
    }
    return findCarrousel;
  }

  async remove(id: UUID) {
    const findCarrousel = await this.findOne(id);

    if ('id' in findCarrousel) {
      if (findCarrousel.image) {
        await this.fileUploadService.deleteFile(findCarrousel.image);
      }

      return this.prisma.carrousels.delete({
        where: {
          id,
        },
        include: {
          author: {
            select: {
              id: true,
              name: true,
              firstname: true,
              UserType: {
                select: {
                  name: true,
                },
              },
            },
          },
        },
      });
    } else {
      return findCarrousel;
    }
  }
}
