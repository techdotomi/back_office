import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseFilePipeBuilder,
  Patch,
  Post,
  Query,
  Req,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { CarrouselsService } from './carrousels.service';
import { CreateCarrouselDto } from './dto/create-carrousel.dto';
import { UpdateCarrouselDto } from './dto/update-carrousel.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { UUID } from 'crypto';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { PaginationDto } from 'src/common/dto/pagination.dto';
import { SortingCarrouselsDto } from './dto/sorting-carrousel.dto';

@Controller('carrousels')
@ApiTags('Carrousels')
export class CarrouselsController {
  constructor(private readonly carrouselsService: CarrouselsService) {}

  @Post()
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(FileInterceptor('image'))
  create(
    @UploadedFile(
      new ParseFilePipeBuilder()
        .addFileTypeValidator({
          fileType: 'jpeg|jpg|png|webp',
        })
        .addMaxSizeValidator({
          maxSize: 100_000_000_000, // 100KB
        })
        .build({
          fileIsRequired: true,
        }),
    )
    image: Express.Multer.File,
    @Body() createCarrouselDto: CreateCarrouselDto,
    @Req() req: Request,
  ) {
    createCarrouselDto.image = image.path;
    return this.carrouselsService.create(createCarrouselDto, req);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  findAll(
    @Query() paginationDto: PaginationDto,
    @Query() sortingDto: SortingCarrouselsDto,
  ) {
    return this.carrouselsService.findAll(paginationDto, sortingDto);
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  findOne(@Param('id') id: string) {
    return this.carrouselsService.findOne(id as UUID);
  }

  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(FileInterceptor('image'))
  @ApiBearerAuth()
  update(
    @Param('id') id: string,
    @UploadedFile(
      new ParseFilePipeBuilder()
        .addFileTypeValidator({
          fileType: 'jpeg|jpg|png|webp',
        })
        .addMaxSizeValidator({
          maxSize: 100_000_000_000, // 100KB
        })
        .build({
          fileIsRequired: false,
        }),
    )
    image: Express.Multer.File,
    @Body() updateCarrouselDto: UpdateCarrouselDto,
  ) {
    if (image) {
      updateCarrouselDto.image = image.path;
    }
    return this.carrouselsService.update(id as UUID, updateCarrouselDto);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  remove(@Param('id') id: string) {
    return this.carrouselsService.remove(id as UUID);
  }
}
