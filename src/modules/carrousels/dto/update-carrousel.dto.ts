import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateCarrouselDto } from './create-carrousel.dto';
import { Transform } from 'class-transformer';
import {
  IsBoolean,
  IsOptional,
  IsString,
  IsUUID,
  Validate,
} from 'class-validator';
import { UserIdExistsRule } from 'src/modules/users/dto/validation-rules/user-id-exists.rule';

export class UpdateCarrouselDto extends PartialType(CreateCarrouselDto) {
  @IsOptional()
  @IsString()
  @ApiProperty({
    name: 'text',
    description: 'The text of the carrousel',
  })
  text: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    name: 'image',
    description: 'The image of the carrousel',
  })
  image: string;

  @IsOptional()
  @IsUUID()
  @Validate(UserIdExistsRule)
  @ApiProperty({
    name: 'authorId',
    description: 'The authorId of the carrousel',
  })
  authorId: string;

  @IsOptional()
  @Transform(({ value }) => value.toString() === 'true')
  @IsBoolean()
  @ApiProperty({
    name: 'active',
    description: 'The active of the carrousel',
  })
  active: boolean;
}
