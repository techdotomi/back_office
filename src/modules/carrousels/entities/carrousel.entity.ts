import { ApiProperty } from '@nestjs/swagger';
import { Carrousels } from '@prisma/client';

export class Carrousel implements Carrousels {
  @ApiProperty()
  id: string;
  
  @ApiProperty()
  text: string;
  
  @ApiProperty()
  image: string;
  
  @ApiProperty()
  authorId: string;
  
  @ApiProperty()
  active: boolean;
  
  @ApiProperty()
  deletedAt: Date | null;
  
  @ApiProperty()
  createdAt: Date;
  
  @ApiProperty()
  updatedAt: Date;
}
