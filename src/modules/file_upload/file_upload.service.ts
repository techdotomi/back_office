import { Injectable, Logger } from '@nestjs/common';
import { unlink } from 'fs/promises';

@Injectable()
export class FileUploadService {
  private readonly logger = new Logger(FileUploadService.name);

  async uploadFiles(files: { [fieldname: string]: Express.Multer.File[] }) {
    const result: { [fieldname: string]: string[] } = {};

    for (const [fieldname, fileArray] of Object.entries(files)) {
      result[fieldname] = fileArray.map((f) => f.path);
    }

    return result;
  }

  async deleteFile(path: string) {
    // Check if file exists
    try {
      await unlink(path);
    } catch (error) {
      this.logger.error(`Error deleting file ${path}: ${error}`);
      return;
    }
    this.logger.log(`File ${path} deleted`);
  }
}
