import {
  Controller,
  Get,
  Post,
  Body,
  HttpCode,
  HttpStatus,
  Query,
  UseInterceptors,
  UploadedFile,
  ParseFilePipeBuilder,
  UseGuards,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { RegisterUserDto } from './dto/register-user.dto';
import {
  ApiBearerAuth,
  ApiBody,
  ApiExtraModels,
  ApiOkResponse,
  ApiTags,
  getSchemaPath,
} from '@nestjs/swagger';
import {
  LoginUserDto,
  LoginUsingEmailDto,
  LoginUsingLoginDto,
  LoginUsingPhoneNumberDto,
} from './dto/login-user.dto';
import { Auth } from './entities/auth.entity';
import {
  ResetPassword,
} from './entities/reset-password.entity';
import {
  RequestResetPasswordDto,
  ResetPasswordDto,
  ResetPasswordUsingEmailDto,
  ResetPasswordUsingPhoneNumberDto,
} from './dto/reset-password.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { JwtAuthGuard } from './guard/jwt-auth.guard';

@Controller('auth')
@ApiTags('Auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/register')
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(FileInterceptor('avatarPicture'))
  register(
    @Body() registerUserDto: RegisterUserDto,
    @UploadedFile(
      new ParseFilePipeBuilder()
        .addFileTypeValidator({
          fileType: 'jpeg|jpg|png|webp',
        })
        .addMaxSizeValidator({
          maxSize: 100_000, // 100KB
        })
        .build({
          fileIsRequired: false,
        }),
    )
    avatarPicture?: Express.Multer.File,
  ) {
    if (avatarPicture) {
      registerUserDto.avatarPicture = avatarPicture.path;
    }
    return this.authService.register(registerUserDto);
  }

  @Post('login')
  @ApiExtraModels(LoginUserDto)
  @ApiOkResponse({ type: Auth })
  @HttpCode(HttpStatus.OK)
  @ApiBody({
    schema: {
      $ref: getSchemaPath(LoginUserDto),
    },
    examples: {
      'Login using email': {
        value: {
          ...new LoginUsingEmailDto(),
        },
      },
      'Login using phone number': {
        value: {
          ...new LoginUsingPhoneNumberDto(),
        },
      },
      'Sign in using login': {
        value: {
          ...new LoginUsingLoginDto(),
        },
      },
    },
  })
  @HttpCode(HttpStatus.OK)
  login(@Body() loginUserDto: LoginUserDto) {
    return this.authService.login(loginUserDto);
  }

  @Get('get_otp')
  @HttpCode(HttpStatus.OK)
  // @ApiOkResponse({ type: RequestResetPassword })
  // @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  resetPassword(@Query() requestResetPasswordDto: RequestResetPasswordDto) {
    return this.authService.sendMailForResetPassword(requestResetPasswordDto);
  }

  @Post('reset-password')
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ type: ResetPassword })
  @ApiBody({
    schema: {
      $ref: '#/components/schemas/ResetPasswordDto',
    },
    examples: {
      'Reset using email': {
        value: {
          ...new ResetPasswordUsingEmailDto(),
        },
      },
      'Reset using phone number': {
        value: {
          ...new ResetPasswordUsingPhoneNumberDto(),
        },
      },
    },
  })
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  changePassword(@Body() resetPasswordDto: ResetPasswordDto) {
    return this.authService.changePassword(resetPasswordDto);
  }
}
