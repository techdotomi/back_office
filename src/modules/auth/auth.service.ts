import {
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { RegisterUserDto } from './dto/register-user.dto';
import { UserTypesService } from '../user_types/user_types.service';
import { UserTypes } from '@prisma/client';
import { UsersService } from '../users/users.service';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { LoginUserDto } from './dto/login-user.dto';
import { PrismaService } from 'src/libs/prisma/prisma.service';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { EmailService } from '../email/email.service';
import { UpdateUserDto } from '../users/dto/update-user.dto';
import { TokenService } from '../token/token.service';
import { UUID } from 'crypto';
import {
  RequestResetPasswordDto,
  ResetPasswordDto,
} from './dto/reset-password.dto';
import { SendMailDto } from '../email/dto/send-email.dto';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  constructor(
    private readonly userTypesService: UserTypesService,
    private readonly usersService: UsersService,
    private readonly prismaService: PrismaService,
    private readonly jwtService: JwtService,
    private readonly emailService: EmailService,
    private readonly tokenService: TokenService,
    private readonly configService: ConfigService,
  ) {}

  async register(registerUserDto: RegisterUserDto) {
    return await this.userTypesService
      .findByName('Standard')
      .then((user_type: UserTypes) => {
        const user: CreateUserDto = {
          ...registerUserDto,
          userTypeId: user_type.id,
        };

        return this.usersService.create(user);
      })
      .catch((error) => {
        console.error('Something is Wrong', error);
      });
  }

  async login(loginUserDto: LoginUserDto) {
    const accessToken = await this.validateUser(loginUserDto);

    if (!accessToken) {
      throw new HttpException('Invalid credentials', HttpStatus.UNAUTHORIZED);
    }
    return accessToken;
  }

  async validateUser(loginUserDto: LoginUserDto) {
    const { password, ...loginInfo } = loginUserDto;
    const user = await this.prismaService.users.findFirst({
      where: {
        ...loginInfo,
      },
    });

    if (!user?.active) {
      throw new UnauthorizedException('This account is unactive.');
    }

    // Use a constant-time comparison to mitigate timing attacks
    const dummyHash =
      '$2b$10$3euPcmQFCiblsZeEu5s7p.9OVHgeHWFDk9nhMqZ0m/3pd/lhwZgES';
    const isGoodPassword = await bcrypt.compare(
      password,
      user?.password ? user.password : dummyHash,
    );

    if (user && isGoodPassword) {
      return {
        data: {
          ...user,
          password: undefined,
        },
        accessToken: this.jwtService.sign({ userId: user.id }),
      };
    }
    return null;
  }

  async sendMailForResetPassword({ email }: RequestResetPasswordDto) {
    const user = await this.usersService.findByEmail({
      email,
    } as UpdateUserDto);

    if (user) {
      const token = await this.tokenService.generatePasswordResetToken(
        user.id as UUID,
      );

      if (typeof token !== 'string') {
        await this.emailService.sendEmail({
          // to: user_created.email,
          // subject: 'Welcome 🥳 to Dotomi App',
          // template: 'welcome',
          // context: {
          //   name: user_created.name,
          //   login_url: this.configService.getOrThrow<string>('APP_URL') + '/auth/login',
          //   year: `${new Date().getFullYear()}`,
          //   support_email: `support@dotomi.app`
          // },
          to: user.email,
          subject: '[RESET PASSWORD] Request for reset Dotomi app password',
          template: 'verification',
          context: {
            name: user.name,
            firstname: user.firstname,
            token: token.token,
            token_formatted: token.token.split(''),
            year: `${new Date().getFullYear()}`,
            support_email: this.configService.get<string>('SUPPORT_EMAIL'),
            app_url: this.configService.get<string>('APP_URL'),
            email: user.email,
            logo:
              this.configService.get<string>('APP_URL') + '/public/logo.png',
          },
          // html: `Your code to reset Dotomi app password is : ${token.token}`,
        } as SendMailDto);
      }
      return 'Check your email you must received a code for reset your password';
    }
    return "Your email didn't exist.";
  }

  async changePassword({
    email,
    phoneNumber,
    token,
    password,
  }: ResetPasswordDto) {
    const user = phoneNumber
      ? await this.usersService.findByPhoneNumber({
          phoneNumber,
        } as UpdateUserDto)
      : await this.usersService.findByEmail({
          email,
        } as UpdateUserDto);

    if (user) {
      const tokenIsValid = await this.tokenService.validatePasswordResetToken(
        user.id as UUID,
        token,
      );
      if (tokenIsValid) {
        await this.usersService.update(
          user.id as UUID,
          {
            password,
          } as UpdateUserDto,
        );
        await this.tokenService.consumePasswordResetToken(user.id as UUID);

        return 'Password changed successfully';
      }
    }
    return 'Invalid token or email';
  }
}
