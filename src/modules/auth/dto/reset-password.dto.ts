import { ApiProperty } from '@nestjs/swagger';
import { Users } from '@prisma/client';
import { IsEmail, IsNotEmpty, IsPhoneNumber, IsString, Matches, ValidateIf } from 'class-validator';

export class RequestResetPasswordDto implements Partial<Users> {
  @IsEmail()
  @IsNotEmpty()
  @ValidateIf((o) => !o.phoneNumber)
  @ApiProperty({
    description: 'The email of the user',
    example: 'johndoe@example.com',
    required: false,
  })
  email: string;

  @IsPhoneNumber()
  @IsNotEmpty()
  @ValidateIf((o) => !o.email)
  @ApiProperty({
    description: 'The phone number of the user',
    example: '+22900000000',
    required: false,
  })
  phoneNumber: string;
}

export class ResetPasswordDto implements Partial<Users> {
  @IsEmail()
  @IsNotEmpty()
  @ValidateIf((o) => !o.phoneNumber)
  @ApiProperty({
    description: 'The email of the user',
    example: 'johndoe@example.com',
  })
  email: string;


  @IsPhoneNumber()
  @IsNotEmpty()
  @ValidateIf((o) => !o.email)
  @ApiProperty({
    description: 'The phone number of the user',
    example: '+22900000000',
  })
  phoneNumber: string;

  @IsNotEmpty()
  // Make a regex to validate the token `XXX-XXX`
  @Matches(/^[a-zA-Z0-9]{3}-[a-zA-Z0-9]{3}$/)
  @ApiProperty({
    description: 'The token sent to the user',
    example: '123-456',
  })
  token: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    description: 'The new password of the user',
    example: 'password',
  })
  password: string;
}


export class ResetPasswordUsingEmailDto implements Partial<Users> {
  @IsEmail()
  @IsNotEmpty()
  @ValidateIf((o) => !o.phoneNumber)
  @ApiProperty({
    description: 'The email of the user',
    example: 'johndoe@example.com',
  })
  email: string = 'johndoe@example.com';

  @IsNotEmpty()
  // Make a regex to validate the token `XXX-XXX`
  @Matches(/^[a-zA-Z0-9]{3}-[a-zA-Z0-9]{3}$/)
  @ApiProperty({
    description: 'The token sent to the user',
    example: '123-456',
  })
  token: string = '123-456';

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    description: 'The new password of the user',
    example: 'password',
  })
  password: string = 'password';
}

export class ResetPasswordUsingPhoneNumberDto implements Partial<Users> {
  @IsNotEmpty()
  @ValidateIf((o) => !o.email)
  @ApiProperty({
    description: 'The phone number of the user',
    example: '+22900000000',
  })
  phoneNumber: string = '+22900000000';

  @IsNotEmpty()
  // Make a regex to validate the token `XXX-XXX`
  @Matches(/^[a-zA-Z0-9]{3}-[a-zA-Z0-9]{3}$/)
  @ApiProperty({
    description: 'The token sent to the user',
    example: '123-456',
  })
  token: string = '123-456';

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    description: 'The new password of the user',
    example: 'password',
  })
  password: string = 'password';
}
