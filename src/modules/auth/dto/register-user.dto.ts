import { Sex, Users } from '@prisma/client';
import {
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsStrongPassword,
  Matches,
  MaxLength,
  MinLength,
} from 'class-validator';
import { Transform } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { EmailNotRegistered } from 'src/modules/users/dto/validation-rules/email-not-registered.rule';
import { PhoneNumberNotRegistered } from 'src/modules/users/dto/validation-rules/phone-number-not-registered.rule';
import { LoginNotRegistered } from 'src/modules/users/dto/validation-rules/login-not-registered.rule';

export class RegisterUserDto
  implements
    Omit<
      Users,
      | 'id'
      | 'deletedAt'
      | 'createdAt'
      | 'updatedAt'
      | 'userTypeId'
      | 'GynecologistInfos'
      | 'FatherEducatorInfos'
      | 'online'
      | 'active'
      | 'remainingCredit'
    >
{
  @IsString()
  @IsNotEmpty()
  @MaxLength(255)
  @ApiProperty({
    maxLength: 255,
    description: 'The name of the user',
    example: 'John',
  })
  name: string;

  @IsString()
  @IsNotEmpty()
  @MaxLength(255)
  @ApiProperty({
    maxLength: 255,
    description: 'The firstname of the user',
    example: 'Doe',
  })
  firstname: string;

  @IsString()
  @IsOptional()
  @MaxLength(255)
  @ApiProperty({
    maxLength: 255,
    description: 'The nickname of the user',
    example: 'johndoe',
  })
  nickname: string | null;

  @IsEmail()
  @IsNotEmpty()
  @EmailNotRegistered({ message: 'Retry with Another Email' })
  @ApiProperty({
    description: 'The email of the user',
    example: 'johndoe@example.com',
  })
  email: string;

  // /^(?:\+22901\s?)?(?:4[0-7]|5[0-9]|6[0-9]|9[013-9])\s?\d{2}\s?\d{4}$/gm
  @Matches(
    /^(?:\+22901\s?)?(?:4[0-7]|5[0-9]|6[0-9]|9[013-9])\s?\d{2}\s?\d{4}$/,
    { message: 'Invalid Phone Number' },
  )
  @IsNotEmpty()
  @ApiProperty({
    description: 'The phone number of the user',
    example: '+22900000000',
  })
  @PhoneNumberNotRegistered({ message: 'Retry with Another Phone Number' })
  phoneNumber: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: 'The login of the user',
    example: 'johndoe',
  })
  @LoginNotRegistered({ message: 'Retry with Another Login' })
  login: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  @MaxLength(255)
  @IsStrongPassword()
  @ApiProperty({
    description: 'The password of the user',
    example: 'password',
  })
  password: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: 'The locality of the user',
    example: 'Cotonou',
  })
  locality: string | null;

  @IsEnum(Sex)
  @Transform(({ value }) => value.toUpperCase() as Sex)
  @IsNotEmpty()
  @ApiProperty({
    description: 'The Sex of user',
    example: 'Female',
  })
  sex: Sex;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'The avatar picture of the user',
    example: 'https://example.com/avatar.jpg',
  })
  avatarPicture: string | null;
}
