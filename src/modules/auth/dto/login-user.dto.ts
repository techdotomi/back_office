import { ApiProperty } from '@nestjs/swagger';
import { Users } from '@prisma/client';
import {
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsPhoneNumber,
  IsString,
} from 'class-validator';

export class LoginUserDto implements Partial<Users> {
  @IsEmail()
  @IsOptional()
  @ApiProperty({
    description: 'The email of the user',
    example: 'johndoe@example.com',
  })
  email: string;

  @IsPhoneNumber()
  @IsOptional()
  @ApiProperty({
    description: 'The phone number of the user',
    example: '+22900000000',
  })
  phoneNumber: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'The login of the user',
    example: 'johndoe',
  })
  login: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    description: 'The password of the user',
    example: 'password',
  })
  password: string;
}

export class LoginUsingEmailDto implements Partial<Users> {
  @IsEmail()
  @IsOptional()
  @ApiProperty({
    description: 'The email of the user',
    example: 'johndoe@example.com',
  })
  email: string = 'johndoe@example.com';

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    description: 'The password of the user',
    example: 'password',
  })
  password: string = 'password';
}

export class LoginUsingPhoneNumberDto implements Partial<Users> {
  @IsPhoneNumber()
  @IsOptional()
  @ApiProperty({
    description: 'The phone number of the user',
    example: '+22900000000',
  })
  phoneNumber: string = '+22900000000';

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    description: 'The password of the user',
    example: 'password',
  })
  password: string = 'password';
}

export class LoginUsingLoginDto implements Partial<Users> {
  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'The login of the user',
    example: 'johndoe',
  })
  login: string = 'johndoe';

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    description: 'The password of the user',
    example: 'password',
  })
  password: string = 'password';
}
