import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { UserTypesModule } from '../user_types/user_types.module';
import { UsersModule } from '../users/users.module';
import { PrismaModule } from 'src/libs/prisma/prisma.module';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { JwtStrategy } from './strategy/jwt.strategy';
import { EmailModule } from '../email/email.module';
import { TokenModule } from '../token/token.module';

import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { generateRandomString } from 'src/libs/utils';
import { extname } from 'path';
import { FileUploadModule } from '../file_upload/file_upload.module';

@Module({
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy],
  imports: [
    UserTypesModule,
    UsersModule,
    PrismaModule,
    FileUploadModule,
    PassportModule,
    EmailModule,
    TokenModule,
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>('JWT_SECRET'),
        signOptions: {
          expiresIn:
            configService.getOrThrow<string>(
              'ACCESS_TOKEN_VALIDITY_DURATION_IN_DAYS',
            ) + 'd' || '30d',
        },
      }),
      inject: [ConfigService],
    }),
    MulterModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        storage: diskStorage({
          destination: configService.get<string>('UPLOAD_PATH'),
          filename: (req, file, cb) => {
            const filename: string = `${file.fieldname}-${Date.now()}-${generateRandomString(10)}${extname(file.originalname)}`;
            cb(null, filename);
          },
        }),
      }),
      inject: [ConfigService],
    }),
  ],
})
export class AuthModule {}
