import { ApiProperty } from '@nestjs/swagger';

export class RequestResetPassword {
  @ApiProperty()
  email: string;
}

export class ResetPassword {
  @ApiProperty()
  token: string;

  @ApiProperty()
  password: string;
}
