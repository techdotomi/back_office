import { ApiProperty } from "@nestjs/swagger";
import { UserTypes } from "@prisma/client";

export class UserType implements UserTypes {
  @ApiProperty()
  id: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  active: boolean;

  @ApiProperty()
  deletedAt: Date | null;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
