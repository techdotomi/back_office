import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserTypeDto } from './dto/create-user_type.dto';
import { UpdateUserTypeDto } from './dto/update-user_type.dto';
import { PrismaService } from 'src/libs/prisma/prisma.service';
import { UUID } from 'crypto';

@Injectable()
export class UserTypesService {
  constructor(private readonly prisma: PrismaService) {}
  async create(createUserTypeDto: CreateUserTypeDto) {
    return this.prisma.userTypes.create({
      data: {
        ...createUserTypeDto,
      },
    });
  }

  async findAll() {
    return this.prisma.userTypes.findMany({
      orderBy: {  
        createdAt: 'desc',  
      },
    });
  }

  async findOne(id: UUID) {
    const user_type = await this.prisma.userTypes.findUnique({
      where: {
        id: id,
      },
    });
    if (!user_type) {
      throw new NotFoundException('UserType with this id does not exist');
    }

    return user_type;
  }

  async findByName(name: string) {
    const user_type = await this.prisma.userTypes.findUnique({
      where: {
        name: name,
      },
    });
    if (!user_type) {
      throw new NotFoundException('UserType with this name does not exist');
    }

    return user_type;
  }

  update(id: UUID, updateUserTypeDto: UpdateUserTypeDto) {
    return this.prisma.userTypes.update({
      where: {
        id,
      },
      data: {
        ...updateUserTypeDto,
      },
    });
  }

  remove(id: UUID) {
    return this.prisma.userTypes.delete({
      where: {
        id,
      },
    });
  }
}
