import { Injectable } from '@nestjs/common';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { UUID } from 'crypto';
import { UserTypesService } from 'src/modules/user_types/user_types.service';

@ValidatorConstraint({ async: true })
@Injectable()
export class UserTypeIdExistsRule implements ValidatorConstraintInterface {
  constructor(private readonly UserTypesService: UserTypesService) {}

  validate(user_type_id: string) {
    if (!user_type_id) return false;
    return this.UserTypesService.findOne(user_type_id as UUID)
      .then((user_type) => {
        return !!(user_type);
      })
      .catch((error) => {
        console.error('Something is Wrong', error);
        return false;
      });
  }

  defaultMessage(): string {
    return 'UserType with this id does not exist';
  }
}
