import { ApiProperty, PartialType } from "@nestjs/swagger";
import { CreateUserTypeDto } from "./create-user_type.dto";
import { IsBoolean, IsOptional, IsString } from "class-validator";
import { Transform } from "class-transformer";

export class UpdateUserTypeDto extends PartialType(CreateUserTypeDto) {
  @IsString()
  @IsOptional()
  @ApiProperty({
    description: "The name of the userType",
    example: true,
  })
  name: string;

  @IsBoolean()
  @IsOptional()
  @Transform(({ value }) => value.toString() === "true")
  @ApiProperty({
    description: "The active status of the userType",
    example: true,
  })
  active: boolean;
}
