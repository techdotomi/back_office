import { ApiProperty } from "@nestjs/swagger";
import { UserTypes } from "@prisma/client";
import { IsBoolean, IsNotEmpty, IsString } from "class-validator";
import { Transform } from "class-transformer";

export class CreateUserTypeDto
  implements Omit<UserTypes, "id" | "deletedAt" | "createdAt" | "updatedAt">
{
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: "The name of the userType",
    example: true,
  })
  name: string;

  @IsBoolean()
  @IsNotEmpty()
  @Transform(({ value }) => value.toString() === "true")
  @ApiProperty({
    description: "The active status of the userType",
    example: true,
  })
  active: boolean;
}
