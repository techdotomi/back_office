import { Module } from '@nestjs/common';
import { UserTypesService } from './user_types.service';
import { UserTypesController } from './user_types.controller';
import { PrismaModule } from 'src/libs/prisma/prisma.module';
import { UserTypeIdExistsRule } from './dto/validation-rules/user_type-id-exists.rule';

@Module({
  controllers: [UserTypesController],
  providers: [UserTypesService, UserTypeIdExistsRule],
  imports: [PrismaModule],
  exports: [UserTypesService]
})
export class UserTypesModule {}
