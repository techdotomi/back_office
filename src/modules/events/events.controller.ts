import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFile,
  ParseFilePipeBuilder,
  HttpStatus,
  HttpCode,
  UseGuards,
  Req,
  Query,
} from '@nestjs/common';
import { EventsService } from './events.service';
import { CreateEventDto } from './dto/create-event.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { UUID } from 'crypto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { Request } from 'express';
import { PaginationDto } from 'src/common/dto/pagination.dto';
import { SortingEventsDto } from './dto/sorting-event.dto';

@Controller('events')
@ApiTags('Events')
export class EventsController {
  constructor(private readonly eventsService: EventsService) {}

  @Post()
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(FileInterceptor('image'))
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  create(
    @UploadedFile(
      new ParseFilePipeBuilder()
        .addFileTypeValidator({
          fileType: 'jpeg|jpg|png|webp',
        })
        .addMaxSizeValidator({
          maxSize: 100_000, // 100KB
        })
        .build({
          fileIsRequired: true,
        }),
    )
    image: Express.Multer.File,
    @Body() createEventDto: CreateEventDto,
    @Req() req: Request,
  ) {
    createEventDto.image = image.path;
    return this.eventsService.create(createEventDto, req);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  findAll(
    @Query() paginationDto: PaginationDto,
    @Query() sortingDto: SortingEventsDto,
  ) {
    return this.eventsService.findAll(paginationDto, sortingDto);
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  findOne(@Param('id') id: string) {
    return this.eventsService.findOne(id as UUID);
  }

  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(FileInterceptor('image'))
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  update(
    @Param('id') id: string,
    @UploadedFile(
      new ParseFilePipeBuilder()
        .addFileTypeValidator({
          fileType: 'jpeg|jpg|png|webp',
        })
        .addMaxSizeValidator({
          maxSize: 100_000, // 100KB
        })
        .build({
          fileIsRequired: false,
        }),
    )
    image: Express.Multer.File,
    @Body() updateEventDto: UpdateEventDto,
  ) {
    if (image) {
      updateEventDto.image = image.path;
    }
    return this.eventsService.update(id as UUID, updateEventDto);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  remove(@Param('id') id: string) {
    return this.eventsService.remove(id as UUID);
  }
}
