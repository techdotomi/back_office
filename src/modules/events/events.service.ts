import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreateEventDto } from './dto/create-event.dto';
import { UpdateEventDto } from './dto/update-event.dto';
import { PrismaService } from 'src/libs/prisma/prisma.service';
import { FileUploadService } from '../file_upload/file_upload.service';
import { UUID } from 'crypto';
import { PaginationDto } from 'src/common/dto/pagination.dto';
import { SortingEventsDto } from './dto/sorting-event.dto';

@Injectable()
export class EventsService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly fileUploadService: FileUploadService,
  ) {}

  async create(createEventDto: CreateEventDto, req: any) {
    const authorId = req.user.id;

    return this.prisma.events.create({
      data: {
        ...createEventDto,
        authorId,
      },
      include: {
        author: {
          select: {
            id: true,
            name: true,
            firstname: true,
            UserType: {
              select: {
                name: true,
              },
            },
          },
        },
      },
    });
  }

  async findAll(paginationDto?: PaginationDto, sortingDto?: SortingEventsDto) {
    const meta = {
      take: paginationDto?.pageSize,
      skip:
        paginationDto?.pageSize && paginationDto?.page
          ? (paginationDto?.page - 1) * paginationDto?.pageSize
          : undefined,
      orderBy: {
        [sortingDto?.sortBy || 'createdAt']: sortingDto?.sortOrder || 'desc',
      },
    };

    const total_events = await this.prisma.events.count();
    const events = await this.prisma.events.findMany({
      include: {
        author: {
          select: {
            id: true,
            name: true,
            firstname: true,
            UserType: {
              select: {
                name: true,
              },
            },
          },
        },
      },
      ...meta,
    });

    return {
      data: events,
      meta: {
        total: total_events,
        page: paginationDto?.page,
        pageSize: paginationDto?.pageSize,
        totalPages: paginationDto?.pageSize
          ? Math.ceil(total_events / paginationDto?.pageSize)
          : undefined,
      },
    };
  }

  async findOne(id: UUID) {
    const event = await this.prisma.events.findUnique({
      where: {
        id: id,
      },
      include: {
        author: {
          select: {
            id: true,
            name: true,
            firstname: true,
            UserType: {
              select: {
                name: true,
              },
            },
          },
        },
      },
    });
    if (!event) {
      throw new NotFoundException('Event with this id does not exist');
    }

    return event;
  }

  async update(id: UUID, updateEventDto: UpdateEventDto) {
    if (Object.keys(updateEventDto).length === 0) {
      throw new HttpException(
        'At least one attribute is required',
        HttpStatus.BAD_REQUEST,
      );
    }

    const findEvent = await this.findOne(id);

    if ('id' in findEvent) {
      if (updateEventDto.image && findEvent.image) {
        this.fileUploadService.deleteFile(findEvent.image);
      }

      const event = await this.prisma.events.update({
        where: {
          id: id,
        },
        data: {
          ...updateEventDto,
        },
        include: {
          author: {
            select: {
              id: true,
              name: true,
              firstname: true,
              UserType: {
                select: {
                  name: true,
                },
              },
            },
          },
        },
      });

      if (!event) {
        throw new HttpException(
          'Event with this id does not exist',
          HttpStatus.NOT_FOUND,
        );
      }

      return event;
    }
    return findEvent;
  }

  async remove(id: UUID) {
    const findEvent = await this.findOne(id);

    if ('id' in findEvent) {
      if (findEvent.image) {
        this.fileUploadService.deleteFile(findEvent.image);
      }

      return this.prisma.events.delete({
        where: {
          id,
        },
        include: {
          author: {
            select: {
              id: true,
              name: true,
              firstname: true,
              UserType: {
                select: {
                  name: true,
                },
              },
            },
          },
        },
      });
    } else {
      return findEvent;
    }
  }
}
