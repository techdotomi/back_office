import { ApiProperty } from '@nestjs/swagger';
import { Events } from '@prisma/client';

export class Event implements Events {
  @ApiProperty()
  id: string;
  
  @ApiProperty()
  text: string;
  
  @ApiProperty()
  image: string;
  
  @ApiProperty()
  authorId: string;
  
  @ApiProperty()
  active: boolean;
  
  @ApiProperty()
  deletedAt: Date | null;
  
  @ApiProperty()
  createdAt: Date;
  
  @ApiProperty()
  updatedAt: Date;
}
