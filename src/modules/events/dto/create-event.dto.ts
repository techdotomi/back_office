import { ApiProperty } from '@nestjs/swagger';
import { Events } from '@prisma/client';
import {
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';

export class CreateEventDto
  implements
    Omit<
      Events,
      'id' | 'deletedAt' | 'createdAt' | 'updatedAt' | 'active' | 'authorId'
    >
{
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    name: 'text',
    description: 'The text of the carrousel',
  })
  text: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    name: 'image',
    description: 'The image of the carrousel',
  })
  image: string;
}
