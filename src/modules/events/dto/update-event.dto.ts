import { PartialType } from '@nestjs/swagger';
import { CreateEventDto } from './create-event.dto';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import {
  IsBoolean,
  IsOptional,
  IsString,
} from 'class-validator';

export class UpdateEventDto extends PartialType(CreateEventDto) {
  @IsOptional()
  @IsString()
  @ApiProperty({
    name: 'text',
    description: 'The text of the event',
  })
  text: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    name: 'image',
    description: 'The image of the event',
  })
  image: string;

  @IsOptional()
  @Transform(({ value }) => (value.toString() === 'true' ? true : false))
  @IsBoolean()
  @ApiProperty({
    name: 'active',
    description: 'The active of the event',
  })
  active: boolean;
}
