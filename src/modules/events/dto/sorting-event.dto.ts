import { IsIn, IsOptional, IsString } from 'class-validator';
import { SortingDto } from 'src/common/dto/pagination.dto';

export class SortingEventsDto extends SortingDto {
  @IsOptional()
  @IsString()
  @IsIn([
    'text',
    'image',
    'authorId',
    'active',
    'deletedAt',
    'createdAt',
    'updatedAt',
  ])
  sortBy?: string = 'createdAt';
}
