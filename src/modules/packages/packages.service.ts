import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { CreatePackageDto } from './dto/create-package.dto';
import { UpdatePackageDto } from './dto/update-package.dto';
import { PrismaService } from 'src/libs/prisma/prisma.service';
import { UUID } from 'crypto';

@Injectable()
export class PackagesService {
  constructor(private readonly prisma: PrismaService) {}
  create(createPackageDto: CreatePackageDto) {
    return this.prisma.packages.create({
      data: {
        ...createPackageDto,
      },
      include: {
        Subscriptions: true,
      },
    });
  }

  findAll() {
    return this.prisma.packages.findMany({
      include: {
        Subscriptions: true,
      },
      orderBy: {  
        createdAt: 'desc',  
      },
    });
  }

  async findOne(id: UUID) {
    const packages = await this.prisma.packages.findUnique({
      where: {
        id,
      },
      include: {
        Subscriptions: true,
      },
    });
    if (packages) {
      return packages;
    }
    throw new NotFoundException('Package not found');
  }

  async update(id: UUID, updatePackageDto: UpdatePackageDto) {
    if (Object.keys(updatePackageDto).length === 0) {
      throw new BadRequestException('No data provided');
    }

    const findPackages = await this.findOne(id);

    if (findPackages) {
      return this.prisma.packages.update({
        where: {
          id,
        },
        data: {
          ...updatePackageDto,
        },
        include: {
          Subscriptions: true,
        },
      });
    }
  }

  async remove(id: UUID) {
    const findPackages = await this.findOne(id);
    if (findPackages) {
      return this.prisma.packages.delete({
        where: {
          id,
        },
      });
    }
  }
}
