import { Injectable } from '@nestjs/common';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { UUID } from 'crypto';
import { NotFoundError } from 'rxjs';
import { PackagesService } from 'src/modules/packages/packages.service';

@ValidatorConstraint({ async: true })
@Injectable()
export class PackageIdExistsRule implements ValidatorConstraintInterface {
  constructor(private readonly PackagesService: PackagesService) {}

  validate(package_id: string) {
    if (!package_id) return false;
    return this.PackagesService.findOne(package_id as UUID)
      .then((packages) => {
        return !!packages;
        // console.log('packages', packages);
        return !(packages instanceof NotFoundError);
        // return packages !== null && packages !== undefined;
      })
      .catch((error) => {
        console.error('Something is Wrong', error);
        return false;
      });
  }

  defaultMessage(): string {
    return 'Package with this id does not exist';
  }
}
