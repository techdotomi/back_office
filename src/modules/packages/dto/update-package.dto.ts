import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreatePackageDto } from './create-package.dto';
import { IsOptional, IsNumber, IsString, IsBoolean, IsDate } from 'class-validator';
import { Decimal } from '@prisma/client/runtime/library';

export class UpdatePackageDto extends PartialType(CreatePackageDto) {
    @IsOptional()
    @IsString()
    @ApiProperty({
      name: 'title',
      description: 'The title of this package',
    })
    title:string;

    @IsOptional()
    @IsString()
    @ApiProperty({
      name: 'description',
      description: 'The description of this package',
    })
    description:string;

    @IsOptional()
    @IsNumber({maxDecimalPlaces: 2})
    @ApiProperty({
      name: 'price',
      description: 'The price of this package',
    })
    price: Decimal;

    @IsOptional()
    @IsNumber()
    @ApiProperty({
      name: 'credit',
      description: 'The credit of this package',
    })
    credit: number;

    @IsOptional()
    @IsBoolean()
    @ApiProperty({
      name: 'active',
      description: 'The active status of this package',
    })
    //@Transform(({ value }) => value === 'true')
    active: boolean;

    @IsOptional()
    @IsDate()
    @ApiProperty({
        name: 'deletedAt',
        description: 'The deletedAt status of this package',
    })
    deletedAt: Date;
}
