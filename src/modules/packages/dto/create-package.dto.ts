import { ApiProperty } from '@nestjs/swagger';
import { Packages } from '@prisma/client';
import { Decimal } from '@prisma/client/runtime/library';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreatePackageDto
  implements
    Omit<Packages, 'id' | 'deletedAt' | 'createdAt' | 'updatedAt' | 'active'>
{
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    name: 'title',
    description: 'The title of this package',
  })
  title: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    name: 'description',
    description: 'The description of this package',
  })
  description: string;

  @IsNotEmpty()
  @IsNumber({maxDecimalPlaces: 2})
  @ApiProperty({
    name: 'price',
    description: 'The price of this package',
  })
  price: Decimal;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty({
    name: 'credit',
    description: 'The credit of this package',
  })
  credit: number;
}
