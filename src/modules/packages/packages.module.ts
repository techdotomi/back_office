import { Module } from '@nestjs/common';
import { PackagesService } from './packages.service';
import { PackagesController } from './packages.controller';
import { PackageIdExistsRule } from './dto/validation-rules/package-id-exists.rule';

@Module({
  controllers: [PackagesController],
  providers: [PackagesService, PackageIdExistsRule],
})
export class PackagesModule {}
