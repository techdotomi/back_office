import { ApiProperty } from "@nestjs/swagger";
import { Packages } from "@prisma/client";
import { Decimal } from "@prisma/client/runtime/library";

export class Package implements Packages {
    @ApiProperty()
    id: string;
    
    @ApiProperty()
    title: string;
    
    @ApiProperty()
    description: string;
    
    @ApiProperty()
    price: Decimal;
    
    @ApiProperty()
    credit: number;
    
    @ApiProperty()
    active: boolean;
    
    @ApiProperty()
    deletedAt: Date | null;
    
    @ApiProperty()
    createdAt: Date;
    
    @ApiProperty()
    updatedAt: Date;
}
