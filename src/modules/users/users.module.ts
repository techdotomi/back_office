import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { PrismaModule } from 'src/libs/prisma/prisma.module';
import { GynecologistInfosService } from 'src/modules/gynecologist_infos/gynecologist_infos.service';
import { UserTypesModule } from 'src/modules/user_types/user_types.module';
import { FatherEducatorInfosService } from 'src/modules/father_educator_infos/father_educator_infos.service';
import { IsEmailNotRegistered } from './dto/validation-rules/email-not-registered.rule';
import { IsPhoneNumberNotRegistered } from './dto/validation-rules/phone-number-not-registered.rule';
import { IsLoginNotRegistered } from './dto/validation-rules/login-not-registered.rule';
import { UserIdExistsRule } from './dto/validation-rules/user-id-exists.rule';

import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { generateRandomString } from 'src/libs/utils';
import { extname } from 'path';
import { FileUploadModule } from '../file_upload/file_upload.module';
import { EmailModule } from '../email/email.module';
import { ChatRoomService } from '../chat_room/chat_room.service';
import { MessageService } from '../message/message.service';

@Module({
  controllers: [UsersController],
  providers: [
    UsersService,
    GynecologistInfosService,
    FatherEducatorInfosService,
    UserIdExistsRule,
    IsEmailNotRegistered,
    IsPhoneNumberNotRegistered,
    IsLoginNotRegistered,
    ChatRoomService,
    MessageService
  ],
  imports: [
    EmailModule,
    PrismaModule,
    UserTypesModule,
    FileUploadModule,
    MulterModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        storage: diskStorage({
          destination: configService.get<string>('UPLOAD_PATH'),
          filename: (req, file, cb) => {
            const filename: string = `${file.fieldname}-${Date.now()}-${generateRandomString(10)}${extname(file.originalname)}`;
            cb(null, filename);
          },
        }),
      }),
      inject: [ConfigService],
    }),
  ],
  exports: [UsersService],
})
export class UsersModule {}
