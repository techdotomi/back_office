import { PartialType } from '@nestjs/mapped-types';
import { IsOptional, IsUUID, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { UpdateGynecologistInfoDto } from '../../gynecologist_infos/dto/update-gynecologist_info.dto';
import { UpdateFatherEducatorInfoDto } from '../../father_educator_infos/dto/update-father_educator_info.dto';
import { UpdateUserDto } from './update-user.dto';

export class UpdateUserTypeDto extends PartialType(UpdateUserDto) {
  @IsUUID()
  @IsOptional()
  @ApiProperty({
    description: 'The user type id of the user',
    example: 'd290f1ee-6c54-4b01-90e6-d701748f0851',
  })
  userTypeId: string;

  @IsOptional()
  @Type(() => UpdateGynecologistInfoDto)
  @ValidateNested()
  @ApiProperty({
    type: UpdateGynecologistInfoDto,
    name: 'GynecologistInfos',
  })
  GynecologistInfos?: UpdateGynecologistInfoDto;

  @IsOptional()
  @Type(() => UpdateFatherEducatorInfoDto)
  @ValidateNested()
  @ApiProperty({
    type: UpdateFatherEducatorInfoDto,
    name: 'FatherEducatorInfos',
  })
  FatherEducatorInfos?: UpdateFatherEducatorInfoDto;
}
