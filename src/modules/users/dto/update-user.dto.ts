import { PartialType } from '@nestjs/mapped-types';
import { CreateUserDto } from './create-user.dto';
import { Sex } from '@prisma/client';
import {
  IsBoolean,
  IsEmail,
  IsEnum,
  IsOptional,
  IsPhoneNumber,
  IsString,
  IsUUID,
  Max,
  MaxLength,
  Min,
  Validate,
  ValidateNested,
} from 'class-validator';
import { Transform, Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { UpdateGynecologistInfoDto } from '../../gynecologist_infos/dto/update-gynecologist_info.dto';
import { UpdateFatherEducatorInfoDto } from '../../father_educator_infos/dto/update-father_educator_info.dto';
import { EmailNotRegistered } from './validation-rules/email-not-registered.rule';
import { PhoneNumberNotRegistered } from './validation-rules/phone-number-not-registered.rule';
import { LoginNotRegistered } from './validation-rules/login-not-registered.rule';
import { UserTypeIdExistsRule } from 'src/modules/user_types/dto/validation-rules/user_type-id-exists.rule';

export class UpdateUserDto extends PartialType(CreateUserDto) {
  @IsString()
  @IsOptional()
  @MaxLength(255)
  @ApiProperty({
    maxLength: 255,
    description: 'The name of the user',
    example: 'John',
  })
  name: string;

  @IsString()
  @IsOptional()
  @MaxLength(255)
  @ApiProperty({
    maxLength: 255,
    description: 'The firstname of the user',
    example: 'Doe',
  })
  firstname: string;

  @IsString()
  @IsOptional()
  @MaxLength(255)
  @ApiProperty({
    maxLength: 255,
    description: 'The nickname of the user',
    example: 'johndoe',
  })
  nickname: string | null;

  @IsEmail()
  @IsOptional()
  @ApiProperty({
    description: 'The email of the user',
    example: 'johndoe@example.com',
  })
  @EmailNotRegistered({ message: 'Retry with Another Email' })
  email: string;

  @IsPhoneNumber()
  @IsOptional()
  @ApiProperty({
    description: 'The phone number of the user',
    example: '+22900000000',
  })
  @PhoneNumberNotRegistered({ message: 'Retry with Another Phone Number' })
  phoneNumber: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'The login of the user',
    example: 'johndoe',
  })
  @LoginNotRegistered({ message: 'Retry with Another Login' })
  login: string;

  @IsString()
  @IsOptional()
  @Min(6)
  @Max(255)
  @ApiProperty({
    description: 'The password of the user',
    example: 'password',
  })
  password: string;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'The locality of the user',
    example: 'Cotonou',
  })
  locality: string | null;

  @IsEnum(Sex)
  @Transform(({ value }) => value as Sex)
  @IsOptional()
  @ApiProperty({
    description: 'The Sex of user',
    example: 'Female',
  })
  sex: Sex;

  // @IsString()
  // @IsOptional()
  // @ApiProperty({
  //   description: 'The avatar picture of the user',
  //   example: 'https://example.com/avatar.jpg',
  // })
  // avatarPicture: string | null;

  @IsUUID()
  @IsOptional()
  @ApiProperty({
    description: 'The user type id of the user',
    example: 'd290f1ee-6c54-4b01-90e6-d701748f0851',
  })
  @Validate(UserTypeIdExistsRule)
  userTypeId: string;

  @IsBoolean()
  @IsOptional()
  @ApiProperty({
    description: 'The online status of the user',
    example: true,
  })
  online: boolean;

  @IsBoolean()
  @IsOptional()
  @ApiProperty({
    description: 'The active status of the user',
    example: true,
  })
  active: boolean;

  @IsOptional()
  @Type(() => UpdateGynecologistInfoDto)
  @ValidateNested()
  @ApiProperty({
    type: UpdateGynecologistInfoDto,
    name: 'GynecologistInfos',
  })
  GynecologistInfos?: UpdateGynecologistInfoDto;

  @IsOptional()
  @Type(() => UpdateFatherEducatorInfoDto)
  @ValidateNested()
  @ApiProperty({
    type: UpdateFatherEducatorInfoDto,
    name: 'FatherEducatorInfos',
  })
  FatherEducatorInfos?: UpdateFatherEducatorInfoDto;
}
