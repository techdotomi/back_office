import { Sex, Users } from '@prisma/client';
import {
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsPhoneNumber,
  IsString,
  IsStrongPassword,
  IsUUID,
  Matches,
  MaxLength,
  MinLength,
  Validate,
  ValidateNested,
} from 'class-validator';
import { Transform, Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { CreateGynecologistInfoDto } from 'src/modules/gynecologist_infos/dto/create-gynecologist_info.dto';
import { CreateFatherEducatorInfoDto } from 'src/modules/father_educator_infos/dto/create-father_educator_info.dto';
import { UserTypeIdExistsRule } from 'src/modules/user_types/dto/validation-rules/user_type-id-exists.rule';
import { EmailNotRegistered } from './validation-rules/email-not-registered.rule';
import { PhoneNumberNotRegistered } from './validation-rules/phone-number-not-registered.rule';
import { LoginNotRegistered } from './validation-rules/login-not-registered.rule';

export class CreateUserDto
  implements
    Omit<
      Users,
      | 'id'
      | 'deletedAt'
      | 'createdAt'
      | 'updatedAt'
      | 'online'
      | 'active'
      | 'remainingCredit'
    >
{
  @IsString()
  @IsNotEmpty()
  @MaxLength(255)
  @ApiProperty({
    maxLength: 255,
    description: 'The name of the user',
    example: 'John',
  })
  name: string;

  @IsString()
  @IsNotEmpty()
  @MaxLength(255)
  @ApiProperty({
    maxLength: 255,
    description: 'The firstname of the user',
    example: 'Doe',
  })
  firstname: string;

  @IsString()
  @IsOptional()
  @MaxLength(255)
  @ApiProperty({
    maxLength: 255,
    description: 'The nickname of the user',
    example: 'johndoe',
  })
  nickname: string | null;

  @IsEmail()
  @IsNotEmpty()
  @EmailNotRegistered({ message: 'Retry with Another Email' })
  @ApiProperty({
    description: 'The email of the user',
    example: 'johndoe@example.com',
  })
  email: string;

  @IsNotEmpty()
  @IsString()
  @Matches(
    /^(?:\+22901\s?)?(?:4[0-7]|5[0-9]|6[0-9]|9[013-9])\s?\d{2}\s?\d{4}$/,
    { message: 'Invalid Phone Number' },
  )
  @ApiProperty({
    description: 'The phone number of the user',
    example: '+22900000000',
  })
  @PhoneNumberNotRegistered({ message: 'Retry with Another Phone Number' })
  phoneNumber: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: 'The login of the user',
    example: 'johndoe',
  })
  @LoginNotRegistered({ message: 'Retry with Another Login' })
  login: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  @MaxLength(255)
  @IsStrongPassword()
  @ApiProperty({
    description: 'The password of the user',
    example: 'password',
  })
  password: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: 'The locality of the user',
    example: 'Cotonou',
  })
  locality: string | null;

  @IsEnum(Sex)
  @Transform(({ value }) => value.toUpperCase() as Sex)
  @IsNotEmpty()
  @ApiProperty({
    description: 'The Sex of user',
    example: 'Female',
  })
  sex: Sex;

  @IsString()
  @IsOptional()
  @ApiProperty({
    description: 'The avatar picture of the user',
    example: 'https://example.com/avatar.jpg',
  })
  avatarPicture: string | null;

  @IsUUID()
  @IsNotEmpty()
  @Validate(UserTypeIdExistsRule)
  @ApiProperty({
    description: 'The user type id of the user',
    example: 'd290f1ee-6c54-4b01-90e6-d701748f0851',
  })
  userTypeId: string;

  @IsOptional()
  @Type(() => CreateGynecologistInfoDto)
  @ValidateNested()
  @ApiProperty({
    type: CreateGynecologistInfoDto,
    name: 'GynecologistInfos',
  })
  GynecologistInfos?: CreateGynecologistInfoDto;

  @IsOptional()
  @Type(() => CreateFatherEducatorInfoDto)
  @ValidateNested()
  @ApiProperty({
    type: CreateFatherEducatorInfoDto,
    name: 'FatherEducatorInfos',
  })
  FatherEducatorInfos?: CreateFatherEducatorInfoDto;
}
