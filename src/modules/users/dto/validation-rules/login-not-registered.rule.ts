import {
  registerDecorator,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { NotFoundError } from 'rxjs';
import { UsersService } from 'src/modules/users/users.service';
import { UpdateUserDto } from '../update-user.dto';
import { Injectable } from '@nestjs/common';

@ValidatorConstraint({ async: true })
@Injectable()
export class IsLoginNotRegistered implements ValidatorConstraintInterface {
  constructor(private readonly usersService: UsersService) {}

  async validate(login: string) {
    try {
      const user = await this.usersService
        .findByLogin({ login } as UpdateUserDto);
      return !(user);
    } catch (error) {
      console.error('Something is Wrong', error);
      return false;
    }
  }
}

export function LoginNotRegistered(validationOptions?: ValidationOptions) {
  return function (object: object, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [],
      validator: IsLoginNotRegistered,
    });
  };
}
