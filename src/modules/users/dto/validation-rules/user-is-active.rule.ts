import { Injectable, Logger } from '@nestjs/common';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { UUID } from 'crypto';
import { UsersService } from 'src/modules/users/users.service';

@ValidatorConstraint({ async: true })
@Injectable()
export class UserIsActiveRule implements ValidatorConstraintInterface {
  constructor(private readonly UserService: UsersService) {}
  private readonly logger = new Logger(UserIsActiveRule.name);

  async validate(user_id: string) {
    if (!user_id) return false;
    try {
      const user = await this.UserService.findOne(user_id as UUID);

      if (user === null || user === undefined) {
        return false;
      }

      return Boolean(user.active);
    } catch (error) {
      this.logger.error('Something is wrong', error);
      return false;
    }
  }

  defaultMessage(): string {
    return 'User with this id does not exist';
  }
}
