import { Injectable } from '@nestjs/common';
import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';
import { UUID } from 'crypto';
import { UsersService } from 'src/modules/users/users.service';

@ValidatorConstraint({ async: true })
@Injectable()
export class UserIdExistsRule implements ValidatorConstraintInterface {
  constructor(private readonly UserService: UsersService) {}

  validate(user_id: string) {
    if (!user_id) return false;
    return this.UserService.findOne(user_id as UUID)
      .then((user) => {
        return user !== null && user !== undefined;
      })
      .catch((error) => {
        console.error('Something is Wrong', error);
        return false;
      });
  }

  defaultMessage(): string {
    return 'User with this id does not exist';
  }
}
