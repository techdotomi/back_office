import { IsIn, IsOptional, IsString } from 'class-validator';
import { SortingDto } from 'src/common/dto/pagination.dto';

export class SortingUsersDto extends SortingDto {
  @IsOptional()
  @IsString()
  @IsIn([
    'id',
    'name',
    'firstname',
    'nickname',
    'email',
    'phoneNumber',
    'login',
    'password',
    'locality',
    'sex',
    'avatarPicture',
    'userTypeId',
    'remainingCredit',
    'online',
    'active',
    'deletedAt',
    'createdAt',
    'updatedAt',
  ])
  sortBy?: string = 'createdAt';
}
