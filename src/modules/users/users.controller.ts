import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  HttpCode,
  HttpStatus,
  UseInterceptors,
  UploadedFile,
  ParseFilePipeBuilder,
  Query,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { User } from './entities/user.entity';
import { UUID } from 'crypto';
import { FileInterceptor } from '@nestjs/platform-express';
import { UpdateUserTypeDto } from './dto/update-user-type.dto';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { ChatRoomService } from '../chat_room/chat_room.service';
import { PaginationDto } from 'src/common/dto/pagination.dto';
import { SortingUsersDto } from './dto/sorting-user.dto';

@Controller('users')
@ApiTags('User')
export class UsersController {
  constructor(private readonly usersService: UsersService,
    private readonly chatRoomService: ChatRoomService
  ) {}

  @Post()
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(FileInterceptor('avatarPicture'))
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse({
    type: User,
    description: 'Create a user',
  })
  create(
    @Body() createUserDto: CreateUserDto,
    @UploadedFile(
      new ParseFilePipeBuilder()
        .addFileTypeValidator({
          fileType: 'jpeg|jpg|png|webp',
        })
        .addMaxSizeValidator({
          maxSize: 100_000, // 100KB
        })
        .build({
          fileIsRequired: false,
        }),
    )
    avatarPicture?: Express.Multer.File,
  ) {
    if (avatarPicture) {
      createUserDto.avatarPicture = avatarPicture.path;
    }

    return this.usersService.create(createUserDto);
  }

  @Get()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  findAll(
    @Query() paginationDto: PaginationDto,
    @Query() sortingDto: SortingUsersDto
  ) {
    return this.usersService.findAll(paginationDto, sortingDto);
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  findOne(@Param('id') id: string) {
    return this.usersService.findOne(id as UUID);
  }

  
  @Get(':id/chats')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  getChats(@Param('id') id: string) {
    return this.chatRoomService.chatRoomByUser(id as UUID);
  }

  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(id as UUID, updateUserDto);
  }

  @Patch(':id/update_user_type')
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse({
    type: User,
    description: 'Update UserType of user',
  })
  updateUserType(
    @Param('id') id: string,
    @Body() updateUserTypeDto: UpdateUserTypeDto,
  ) {
    return this.usersService.updateUserType(id as UUID, updateUserTypeDto);
  }

  @Patch(':id/update_avatar_picture')
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(FileInterceptor('avatarPicture'))
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  updateAvatarPicture(
    @Param('id') id: string,
    @UploadedFile(
      new ParseFilePipeBuilder()
        .addFileTypeValidator({
          fileType: 'jpeg|jpg|png|webp',
        })
        .addMaxSizeValidator({
          maxSize: 100_000, // 100KB
        })
        .build({
          fileIsRequired: true,
        }),
    )
    avatarPicture: Express.Multer.File,
  ) {
    return this.usersService.updateAvatarPicture(
      id as UUID,
      avatarPicture.path,
    );
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse({
    type: User,
    description: 'Delete user',
  })
  remove(@Param('id') id: string) {
    return this.usersService.remove(id as UUID);
  }
}
