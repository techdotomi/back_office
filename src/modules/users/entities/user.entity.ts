import { ApiProperty } from "@nestjs/swagger";
import { Sex, Users } from "@prisma/client";

export class User implements Omit<Users, "password"> {
  @ApiProperty()
  id: string;

  @ApiProperty()
  name: string;

  @ApiProperty()
  firstname: string;

  @ApiProperty()
  nickname: string | null;

  @ApiProperty()
  email: string;

  @ApiProperty()
  phoneNumber: string;

  @ApiProperty()
  login: string;

  @ApiProperty()
  password: string;

  @ApiProperty()
  locality: string | null;

  @ApiProperty()
  sex: Sex;

  @ApiProperty()
  avatarPicture: string | null;

  @ApiProperty()
  userTypeId: string;

  @ApiProperty()
  online: boolean;

  @ApiProperty()
  active: boolean;

  @ApiProperty()
  remainingCredit: number;

  @ApiProperty()
  deletedAt: Date | null;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
