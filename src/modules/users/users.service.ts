import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { PrismaService } from 'src/libs/prisma/prisma.service';
import { UUID } from 'crypto';
import * as bcrypt from 'bcrypt';
import { GynecologistInfosService } from 'src/modules/gynecologist_infos/gynecologist_infos.service';
import { FatherEducatorInfosService } from 'src/modules/father_educator_infos/father_educator_infos.service';
import { FileUploadService } from '../file_upload/file_upload.service';
import { UpdateUserTypeDto } from './dto/update-user-type.dto';
import { EmailService } from '../email/email.service';
import { ConfigService } from '@nestjs/config';
import { SortingUsersDto } from './dto/sorting-user.dto';
import { PaginationDto } from 'src/common/dto/pagination.dto';

@Injectable()
export class UsersService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly gynecologistInfosService: GynecologistInfosService,
    private readonly fatherEducatorInfosService: FatherEducatorInfosService,
    private readonly fileUploadService: FileUploadService,
    private readonly emailService: EmailService,
    private readonly configService: ConfigService,
  ) {}

  private readonly logger = new Logger(UsersService.name);

  async create(createUserDto: CreateUserDto) {
    // Get UserType
    const userType = await this.prisma.userTypes.findUnique({
      where: {
        id: createUserDto.userTypeId,
      },
    });

    if (!userType) {
      throw new HttpException('UserType not found', HttpStatus.NOT_FOUND);
    }

    createUserDto.password = await bcrypt.hash(createUserDto.password, 10);

    if (userType.name.toLowerCase() === 'gynecologue') {
      if (!createUserDto.GynecologistInfos) {
        throw new HttpException(
          'GynecologistInfo is required',
          HttpStatus.BAD_REQUEST,
        );
      }

      // Split gynecologistInfos to another attribute of user
      const { GynecologistInfos, ...user } = createUserDto;

      const user_created = await this.prisma.users.create({
        data: {
          ...user,
          FatherEducatorInfos: undefined, // Remove FatherEducatorInfos property if exist
          GynecologistInfos: {
            create: GynecologistInfos,
          },
        },
        include: {
          GynecologistInfos: true,
        },
      });

      await this.emailService.sendEmail({
        to: user_created.email,
        subject: 'Welcome 🥳 to Dotomi App',
        template: 'welcome',
        context: {
          name: user_created.name,
          login_url:
            this.configService.getOrThrow<string>('APP_URL') + '/auth/login',
          year: `${new Date().getFullYear()}`,
          support_email: `support@dotomi.app`,
          logo: this.configService.get<string>('APP_URL') + '/public/logo.png',
        },
      });

      return user_created;
    } else if (userType.name.toLowerCase() === 'père éducateur') {
      if (!createUserDto.FatherEducatorInfos) {
        throw new HttpException(
          'FatherEducatorInfo is required',
          HttpStatus.BAD_REQUEST,
        );
      }

      // Split fatherEducatorInfos to another attribute of user
      const { FatherEducatorInfos, ...user } = createUserDto;

      const user_created = await this.prisma.users.create({
        data: {
          ...user,
          GynecologistInfos: undefined,
          FatherEducatorInfos: {
            create: FatherEducatorInfos,
          },
        },
        include: {
          FatherEducatorInfos: true,
        },
      });

      await this.emailService.sendEmail({
        to: user_created.email,
        subject: 'Welcome 🥳 to Dotomi App',
        template: 'welcome',
        context: {
          name: user_created.name,
          login_url:
            this.configService.getOrThrow<string>('APP_URL') + '/auth/login',
          year: `${new Date().getFullYear()}`,
          support_email: `support@dotomi.app`,
          logo: this.configService.get<string>('APP_URL') + '/public/logo.png',
        },
      });

      return user_created;
    } else {
      const user_created = await this.prisma.users.create({
        data: {
          ...createUserDto,
          GynecologistInfos: undefined,
          FatherEducatorInfos: undefined,
        },
      });

      await this.emailService.sendEmail({
        to: user_created.email,
        subject: 'Welcome 🥳 to Dotomi App',
        template: 'welcome',
        context: {
          name: user_created.name,
          login_url:
            this.configService.getOrThrow<string>('APP_URL') + '/auth/login',
          year: `${new Date().getFullYear()}`,
          support_email: `support@dotomi.app`,
          logo: this.configService.get<string>('APP_URL') + '/public/logo.png',
        },
      });

      return user_created;
    }
  }

  async findAll(paginationDto?: PaginationDto, sortingDto?: SortingUsersDto) {
    const meta = {
      take: paginationDto?.pageSize,
      skip:
        paginationDto?.pageSize && paginationDto?.page
          ? (paginationDto?.page - 1) * paginationDto?.pageSize
          : undefined,
      orderBy: {
        [sortingDto?.sortBy || 'createdAt']: sortingDto?.sortOrder || 'desc',
      },
    };

    const total_users = await this.prisma.users.count();

    const users = await this.prisma.users.findMany({
      select: {
        id: true,
        name: true,
        firstname: true,
        nickname: true,
        email: true,
        phoneNumber: true,
        login: true,
        password: false,
        locality: true,
        sex: true,
        avatarPicture: true,
        userTypeId: true,
        online: true,
        active: true,
        deletedAt: true,
        createdAt: true,
        updatedAt: true,
      },
      ...meta,
    });

    if (users.length == 0) throw new NotFoundException('No record of users.');

    return {
      data: users,
      meta: {
        total: total_users,
        page: paginationDto?.page,
        pageSize: paginationDto?.pageSize,
        totalPages: paginationDto?.pageSize
          ? Math.ceil(total_users / paginationDto?.pageSize)
          : undefined,
      },
    };
  }

  async findOne(id: UUID) {
    try {
      return this.prisma.users.findUnique({
        where: {
          id,
        },
        include: {
          FatherEducatorInfos: true,
          GynecologistInfos: true,
        },
      });
    } catch (NotFoundError) {
      this.logger.error(
        `Failed to find user by id: ${id}. Error: ${NotFoundError.message}`,
        NotFoundError.stack,
      );
      throw new NotFoundException('User with this id does not exist');
    }
  }

  async findByEmail({ email }: UpdateUserDto) {
    try {
      return this.prisma.users.findUnique({
        where: {
          email: email,
        },
      });
    } catch (error) {
      this.logger.error(
        `Failed to find user by email: ${email}. Error: ${error.message}`,
        error.stack,
      );
      throw new NotFoundException('User with this email does not exist');
    }
  }

  async findByPhoneNumber({ phoneNumber }: UpdateUserDto) {
    try {
      return this.prisma.users.findUnique({
        where: {
          phoneNumber: phoneNumber,
        },
      });
    } catch (error) {
      this.logger.error(
        `Failed to find user by phoneNumber: ${phoneNumber}. Error: ${error.message}`,
        error.stack,
      );
      throw new NotFoundException('User with this phoneNumber does not exist');
    }
  }

  async findByLogin({ login }: UpdateUserDto) {
    try {
      return this.prisma.users.findUnique({
        where: {
          login: login,
        },
      });
    } catch (error) {
      this.logger.error(
        `Failed to find user by login: ${login}. Error: ${error.message}`,
        error.stack,
      );
      throw new NotFoundException('User with this login does not exist');
    }
  }

  async update(id: UUID, updateUserDto: UpdateUserDto) {
    // Check if updateUserDto have one or more attributes
    if (Object.keys(updateUserDto).length === 0) {
      throw new HttpException(
        'At least one attribute is required',
        HttpStatus.BAD_REQUEST,
      );
    }

    if (updateUserDto.password)
      updateUserDto.password = await bcrypt.hash(updateUserDto.password, 10);

    // Get users data
    const users = await this.prisma.users.findUnique({
      where: {
        id,
      },
      include: {
        FatherEducatorInfos: true,
        GynecologistInfos: true,
        UserType: true,
      },
    });

    if (!users) {
      throw new HttpException("This User doesn't exist", HttpStatus.NOT_FOUND);
    }

    if (updateUserDto.avatarPicture && users.avatarPicture) {
      await this.fileUploadService.deleteFile(users.avatarPicture);
    }

    if (updateUserDto.userTypeId) {
      const { FatherEducatorInfos, GynecologistInfos, userTypeId, ...user } =
        updateUserDto;

      await this.updateUserType(id, {
        userTypeId: userTypeId,
        FatherEducatorInfos: FatherEducatorInfos,
        GynecologistInfos: GynecologistInfos,
      } as UpdateUserTypeDto);

      return this.prisma.users.update({
        where: {
          id,
        },
        data: {
          userTypeId: userTypeId,
          ...user,
        },
      });
    } else {
      // In case, userType not changed
      const { FatherEducatorInfos, GynecologistInfos, ...user } = updateUserDto;
      return this.prisma.users.update({
        where: {
          id: id,
        },
        data: {
          ...user,
          FatherEducatorInfos: FatherEducatorInfos
            ? {
                update: {
                  ...FatherEducatorInfos,
                },
              }
            : undefined,
          GynecologistInfos: GynecologistInfos
            ? {
                update: {
                  ...GynecologistInfos,
                },
              }
            : undefined,
        },
      });
    }
  }

  async updateUserType(id: UUID, updateUserTypeDto: UpdateUserTypeDto) {
    if (!updateUserTypeDto?.userTypeId) {
      this.logger.error('UserTypeId is required', 'UserTypeId is required');
      throw new BadRequestException('UserTypeId is required');
    }

    // Get users data
    const users = await this.prisma.users.findUnique({
      where: {
        id,
      },
      include: {
        FatherEducatorInfos: true,
        GynecologistInfos: true,
        UserType: true,
      },
    });

    if (!users) {
      this.logger.error(`This User doesn't exist ${id}`);
      throw new BadRequestException(`This User doesn't exist ${id}`);
    }

    if (updateUserTypeDto.userTypeId) {
      // Get UserType
      const userType = await this.prisma.userTypes.findUnique({
        where: {
          id: updateUserTypeDto.userTypeId as UUID,
        },
      });

      if (!userType) {
        this.logger.error(`This User doesn't exist ${id}`);
        throw new BadRequestException(`This User doesn't exist ${id}`);
      }

      // Check if userType changed
      if (users?.userTypeId !== updateUserTypeDto.userTypeId) {
        // Check if userType was "gynecologue" or "père éducateur" and delete GynecologistInfos or FatherEducatorInfos
        if (users?.UserType.name.toLowerCase() === 'gynecologue') {
          // Delete GynecologistInfos
          await this.prisma.users.update({
            where: {
              id: id,
            },
            data: {
              GynecologistInfos: {
                delete: true,
              },
            },
          });
        } else if (users?.UserType.name.toLowerCase() === 'père éducateur') {
          // Delete FatherEducatorInfos
          await this.prisma.users.update({
            where: {
              id: id,
            },
            data: {
              FatherEducatorInfos: {
                delete: true,
              },
            },
          });
        }

        // Check if userType is "gynecologue" or "père éducateur"
        // and create GynecologistInfos or FatherEducatorInfos and update user
        if (userType.name.toLowerCase() === 'gynecologue') {
          if (!updateUserTypeDto.GynecologistInfos) {
            this.logger.error('GynecologistInfo is required');
            throw new BadRequestException('GynecologistInfo is required');
          }
          // Split gynecologistInfos to another attribute of user
          const { GynecologistInfos } = updateUserTypeDto;
          // TODO: Use gynecologistInfosService to create GynecologistInfo
          return this.prisma.users.update({
            where: {
              id: id,
            },
            data: {
              FatherEducatorInfos: undefined,
              GynecologistInfos: {
                create: GynecologistInfos,
              },
              userTypeId: updateUserTypeDto.userTypeId,
            },
            include: {
              FatherEducatorInfos: true,
              GynecologistInfos: true,
            },
          });
        } else if (userType.name.toLowerCase() === 'père éducateur') {
          if (!updateUserTypeDto.FatherEducatorInfos) {
            this.logger.error('FatherEducatorInfo is required');
            throw new BadRequestException('FatherEducatorInfo is required');
          }
          // Split fatherEducatorInfos to another attribute of user
          const { FatherEducatorInfos } = updateUserTypeDto;
          return this.prisma.users.update({
            where: {
              id: id,
            },
            data: {
              GynecologistInfos: undefined,
              FatherEducatorInfos: {
                create: FatherEducatorInfos,
              },
              userTypeId: updateUserTypeDto.userTypeId,
            },
            include: {
              FatherEducatorInfos: true,
              GynecologistInfos: true,
            },
          });
        } else {
          return this.prisma.users.update({
            where: {
              id: id,
            },
            data: {
              GynecologistInfos: undefined,
              FatherEducatorInfos: undefined,
              userTypeId: updateUserTypeDto.userTypeId,
            },
            include: {
              FatherEducatorInfos: true,
              GynecologistInfos: true,
            },
          });
        }
      }
    }
  }

  async updateAvatarPicture(id: UUID, avatarPicture: string) {
    await this.prisma.users.update({
      where: {
        id,
      },
      data: {
        avatarPicture,
      },
    });
    this.logger.log(`Avatar picture updated successfully by User ID: ${id}`);
    return 'Done';
  }

  async addCredit(id: UUID, remainingCredit: number) {
    const user = await this.findOne(id);

    if (user) {
      return this.prisma.users.update({
        where: {
          id: user.id,
        },
        data: {
          remainingCredit: user.remainingCredit + remainingCredit,
        },
      });
    } else {
      this.logger.error(`Failed to add credit to user by id: ${id}`);
      throw new NotFoundException('User with this id does not exist');
    }
  }

  async remove(id: UUID) {
    const user = await this.findOne(id);

    if (user) {
      // if (user.avatarPicture) {
      //   this.fileUploadService.deleteFile(user.avatarPicture);
      // }

      // if (user?.FatherEducatorInfos) {
      //   await this.fatherEducatorInfosService.remove(
      //     user?.FatherEducatorInfos.id as UUID,
      //   );
      // }

      // if (user?.GynecologistInfos) {
      //   await this.gynecologistInfosService.remove(
      //     user?.GynecologistInfos.id as UUID,
      //   );
      // }

      return this.prisma.users.update({
        where: {
          id,
        },
        data: {
          active: false,
        },
      });
    }

    this.logger.error(`Failed to remove user by id: ${id}`);
    throw new NotFoundException('User with this id does not exist');
  }
}
