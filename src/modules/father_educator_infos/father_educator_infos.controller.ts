import {
  Controller,
  Get,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { FatherEducatorInfosService } from './father_educator_infos.service';
import { UUID } from 'crypto';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { ApiBearerAuth } from '@nestjs/swagger';

@Controller('father-educator-infos')
export class FatherEducatorInfosController {
  constructor(
    private readonly fatherEducatorInfosService: FatherEducatorInfosService,
  ) {}

  @Get()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  findAll() {
    return this.fatherEducatorInfosService.findAll();
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  findOne(@Param('id') id: string) {
    return this.fatherEducatorInfosService.findOne(id as UUID);
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  remove(@Param('id') id: string) {
    return this.fatherEducatorInfosService.remove(id as UUID);
  }
}
