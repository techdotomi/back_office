import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateFatherEducatorInfoDto } from './dto/create-father_educator_info.dto';
import { UpdateFatherEducatorInfoDto } from './dto/update-father_educator_info.dto';
import { PrismaService } from 'src/libs/prisma/prisma.service';
import { Users } from '@prisma/client';
import { UUID } from 'crypto';

@Injectable()
export class FatherEducatorInfosService {
  constructor(private readonly prisma: PrismaService) {}
  async create(
    createFatherEducatorInfoDto: CreateFatherEducatorInfoDto,
    user: Users,
  ) {
    return this.prisma.fatherEducatorInfos.create({
      data: {
        ...createFatherEducatorInfoDto,
        user: {
          connect: {
            id: user.id,
          },
        },
      },
    });
  }

  findAll() {
    return `This action returns all fatherEducatorInfos`;
  }

  async findOne(id: UUID) {
    return this.prisma.fatherEducatorInfos.findUnique({
      where: {
        id,
      },
      include: {
        user: true,
      },
    });
  }

  update(id: UUID, updateFatherEducatorInfoDto: UpdateFatherEducatorInfoDto) {
    return this.prisma.fatherEducatorInfos.update({
      where: {
        id,
      },
      data: {
        ...updateFatherEducatorInfoDto,
      },
    });
  }

  async remove(id: UUID) {
    const fatherEducatorInfo = await this.prisma.fatherEducatorInfos.findUnique(
      {
        where: {
          id,
        },
        include: {
          user: true,
        },
      },
    );
    if (!fatherEducatorInfo) {
      throw new NotFoundException('FatherEducatorInfo not found');
    }

    return this.prisma.fatherEducatorInfos.update({
      where: {
        id,
      },
      data: {
        active: false,
        deletedAt: new Date(),
      },
    });
  }
}
