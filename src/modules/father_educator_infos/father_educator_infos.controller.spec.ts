import { Test, TestingModule } from '@nestjs/testing';
import { FatherEducatorInfosController } from './father_educator_infos.controller';
import { FatherEducatorInfosService } from './father_educator_infos.service';

describe('FatherEducatorInfosController', () => {
  let controller: FatherEducatorInfosController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FatherEducatorInfosController],
      providers: [FatherEducatorInfosService],
    }).compile();

    controller = module.get<FatherEducatorInfosController>(FatherEducatorInfosController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
