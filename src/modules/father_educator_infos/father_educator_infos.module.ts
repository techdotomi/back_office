import { Module } from '@nestjs/common';
import { FatherEducatorInfosService } from './father_educator_infos.service';
import { PrismaModule } from 'src/libs/prisma/prisma.module';

@Module({
  providers: [FatherEducatorInfosService],
  imports: [PrismaModule]
})
export class FatherEducatorInfosModule {}
