import { PartialType } from "@nestjs/mapped-types";
import { CreateFatherEducatorInfoDto } from "./create-father_educator_info.dto";
import {
  IsBoolean,
  IsEmail,
  IsOptional,
  IsPhoneNumber,
  IsString,
} from "class-validator";
import { Transform } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";

export class UpdateFatherEducatorInfoDto extends PartialType(
  CreateFatherEducatorInfoDto,
) {
  @IsOptional()
  @IsString()
  @ApiProperty({
    name: "education",
  })
  education: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    name: "profession",
  })
  profession: string;

  @IsOptional()
  @IsPhoneNumber()
  @ApiProperty({
    name: "officePhoneNumber",
  })
  officePhoneNumber: string;

  @IsOptional()
  @IsEmail()
  @ApiProperty({
    name: "officeEmail",
  })
  officeEmail: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    name: "officeAddress",
  })
  officeAddress: string;

  @IsOptional()
  @Transform(({ value }) => value.toString() === "true")
  @IsBoolean()
  @ApiProperty({
    name: "active",
  })
  active: boolean;

  // @IsOptional()
  // @IsString()
  // @ApiProperty({
  //   name: "userId",
  // })
  // userId: string;
}
