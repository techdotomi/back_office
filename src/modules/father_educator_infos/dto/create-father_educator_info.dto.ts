import { FatherEducatorInfos } from "@prisma/client";
import {
  IsBoolean,
  IsEmail,
  IsNotEmpty,
  IsPhoneNumber,
  IsString,
} from "class-validator";
import { Transform } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";
export class CreateFatherEducatorInfoDto
  implements
    Omit<
      FatherEducatorInfos,
      "id" | "userId" | "deletedAt" | "createdAt" | "updatedAt"
    >
{
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    name: "education",
  })
  education: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    name: "profession",
  })
  profession: string;

  @IsNotEmpty()
  @IsPhoneNumber()
  @ApiProperty({
    name: "officePhoneNumber",
  })
  officePhoneNumber: string;

  @IsNotEmpty()
  @IsEmail()
  @ApiProperty({
    name: "officeEmail",
  })
  officeEmail: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    name: "officeAddress",
  })
  officeAddress: string;

  @IsNotEmpty()
  @Transform(({ value }) => (value.toString() === "true" ? true : false))
  @IsBoolean()
  @ApiProperty({
    name: "active",
  })
  active: boolean;

  // @IsNotEmpty()
  // @IsString()
  // @ApiProperty({
  //   name: "userId",
  // })
  // userId: string;
}
