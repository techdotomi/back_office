import { Test, TestingModule } from '@nestjs/testing';
import { FatherEducatorInfosService } from './father_educator_infos.service';

describe('FatherEducatorInfosService', () => {
  let service: FatherEducatorInfosService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FatherEducatorInfosService],
    }).compile();

    service = module.get<FatherEducatorInfosService>(FatherEducatorInfosService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
