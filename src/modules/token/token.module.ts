import { Module } from '@nestjs/common';
import { PrismaModule } from 'src/libs/prisma/prisma.module';
import { TokenService } from './token.service';

@Module({
  providers: [TokenService],
  imports: [PrismaModule],
  exports: [TokenService]
})
export class TokenModule {}
