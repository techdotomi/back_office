import { Injectable } from '@nestjs/common';
import { TokenType } from '@prisma/client';
import { UUID } from 'crypto';
import { PrismaService } from 'src/libs/prisma/prisma.service';
import { generateRandomString } from 'src/libs/utils';

@Injectable()
export class TokenService {
  constructor(private readonly prismaService: PrismaService) {}

  async generatePasswordResetToken(userId: UUID | null) {
    const token = `${generateRandomString(3)}-${generateRandomString(3)}`;
    const expirationDate = new Date();
    expirationDate.setHours(expirationDate.getHours() + 1);

    if (!userId) {
      return token;
    }

    await this.consumePasswordResetToken(userId);

    return this.prismaService.tokens.create({
      data: {
        token,
        expiredAt: expirationDate,
        type: TokenType.PASSWORD_RESET,
        User: {
          connect: {
            id: userId,
          },
        },
      },
    });
  }

  async validatePasswordResetToken(userId: UUID, token: string) {
    const tokenRecord = await this.prismaService.tokens.findFirst({
      where: {
        token,
        userId,
        type: TokenType.PASSWORD_RESET,
        expiredAt: {
          gte: new Date(),
        },
      },
    });

    return !!tokenRecord;
  }

  async consumePasswordResetToken(userId: UUID) {
    await this.prismaService.tokens.updateMany({
      where: {
        userId,
        type: TokenType.PASSWORD_RESET,
      },
      data: {
        expiredAt: new Date(),
      },
    });
  }
}
