import { Controller, Get, Post, Body, Patch, Param, Delete, HttpCode, HttpStatus, UseGuards, Query } from '@nestjs/common';
import { SubscriptionsService } from './subscriptions.service';
import { CreateSubscriptionDto } from './dto/create-subscription.dto';
import { UpdateSubscriptionDto } from './dto/update-subscription.dto';
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { Subscription } from './entities/subscription.entity';
import { UUID } from 'crypto';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { PaginationDto } from 'src/common/dto/pagination.dto';
import { SortingSubscriptionsDto } from './dto/sorting-subscriptions.dto';

@Controller('subscriptions')
@ApiTags('subscriptions')
export class SubscriptionsController {
  constructor(private readonly subscriptionsService: SubscriptionsService) {}

  @Post()
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({
    type: Subscription,
    description: 'Create Subscription',
  })
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  create(@Body() createSubscriptionDto: CreateSubscriptionDto) {
    return this.subscriptionsService.create(createSubscriptionDto);
  }

  @Get()
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  findAll(
    @Query() paginationDto: PaginationDto,
    @Query() sortingDto: SortingSubscriptionsDto,
  ) {
    return this.subscriptionsService.findAll(paginationDto, sortingDto);
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  findOne(@Param('id') id: string) {
    return this.subscriptionsService.findOne(id as UUID);
  }

  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  update(@Param('id') id: string, @Body() updateSubscriptionDto: UpdateSubscriptionDto) {
    return this.subscriptionsService.update(id as UUID, updateSubscriptionDto);
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  remove(@Param('id') id: string) {
    return this.subscriptionsService.remove(id as UUID);
  }
}
