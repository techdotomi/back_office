import { Module } from '@nestjs/common';
import { SubscriptionsService } from './subscriptions.service';
import { SubscriptionsController } from './subscriptions.controller';
import { UsersService } from '../users/users.service';
import { GynecologistInfosService } from '../gynecologist_infos/gynecologist_infos.service';
import { FatherEducatorInfosService } from '../father_educator_infos/father_educator_infos.service';
import { FileUploadService } from '../file_upload/file_upload.service';
import { EmailService } from '../email/email.service';

@Module({
  controllers: [SubscriptionsController],
  providers: [
    SubscriptionsService,
    UsersService,
    GynecologistInfosService,
    FatherEducatorInfosService,
    FileUploadService,
    EmailService
  ],
})
export class SubscriptionsModule {}
