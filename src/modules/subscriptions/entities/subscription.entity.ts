import { ApiProperty } from '@nestjs/swagger';
import { Subscriptions } from '@prisma/client';

export class Subscription implements Subscriptions {
  @ApiProperty()
  id: string;

  @ApiProperty()
  packageId: string;

  @ApiProperty()
  transactionId: string;

  @ApiProperty()
  remainingCredit: number;

  @ApiProperty()
  isActive: boolean;

  @ApiProperty()
  deletedAt: Date | null;
  
  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
