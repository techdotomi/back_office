import {
  HttpException,
  HttpStatus,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { CreateSubscriptionDto } from './dto/create-subscription.dto';
import { UpdateSubscriptionDto } from './dto/update-subscription.dto';
import { PrismaService } from 'src/libs/prisma/prisma.service';
import { UUID } from 'crypto';
import { UsersService } from '../users/users.service';
import { PaginationDto } from 'src/common/dto/pagination.dto';
import { SortingSubscriptionsDto } from './dto/sorting-subscriptions.dto';

@Injectable()
export class SubscriptionsService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly usersService: UsersService,
  ) {}

  private readonly logger = new Logger(SubscriptionsService.name);
  async create(createSubscriptionDto: CreateSubscriptionDto) {
    // Get the credit of the package and assign it to the remainingCredit of the subscription
    const packageCredit = await this.prisma.packages.findUnique({
      where: {
        id: createSubscriptionDto.packageId,
      },
      select: {
        credit: true,
      },
    });

    if (!packageCredit) {
      throw new NotFoundException('Package not found');
    }

    const subscription = await this.prisma.subscriptions.create({
      data: {
        ...createSubscriptionDto,
        remainingCredit: packageCredit?.credit,
      },
      include: {
        package: true,
        transaction: {
          select: {
            aggregator: {
              select: {
                id: true,
                name: true,
              },
            },
            price: true,
            status: true,
            date: true,
            user: {
              select: {
                id: true,
                email: true,
                name: true,
                nickname: true,
                phoneNumber: true,
                remainingCredit: true,
                UserType: {
                  select: {
                    id: true,
                    name: true,
                  },
                },
              },
            },
          },
        },
      },
    });

    // Make last subscription inactive if user.remainingCredit isn't greater than 0
    if (subscription.transaction.user.remainingCredit <= 0) {
      const subscriptions = await this.prisma.subscriptions.findMany({
        where: {
          transaction: {
            userId: subscription.transaction.user.id,
          },
        },
        orderBy: {
          createdAt: 'desc',
        },
      });

      if (subscriptions.length > 1) {
        await this.prisma.subscriptions.update({
          where: {
            id: subscriptions[1].id,
          },
          data: {
            isActive: false,
          },
        });
        this.logger.log(
          `Last subscription ${subscriptions[1].id} is now inactive`,
        );
      }
    }

    await this.usersService.addCredit(
      subscription.transaction.user.id as UUID,
      subscription.package.credit,
    );

    return this.findOne(subscription.id as UUID);
  }

  async findAll(paginationDto?: PaginationDto, sortingDto?: SortingSubscriptionsDto) {
    const meta = {
      take: paginationDto?.pageSize,
      skip:
        paginationDto?.pageSize && paginationDto?.page
          ? (paginationDto?.page - 1) * paginationDto?.pageSize
          : undefined,
      orderBy: {
        [sortingDto?.sortBy || 'createdAt']: sortingDto?.sortOrder || 'desc',
      },
    };
    const total_subscriptions = await this.prisma.subscriptions.count();

    const subscriptions = await this.prisma.subscriptions.findMany({
      include: {
        package: true,
        transaction: {
          select: {
            aggregator: {
              select: {
                id: true,
                name: true,
              },
            },
            price: true,
            status: true,
            date: true,
            user: {
              select: {
                id: true,
                email: true,
                name: true,
                nickname: true,
                phoneNumber: true,
                remainingCredit: true,
                UserType: {
                  select: {
                    id: true,
                    name: true,
                  },
                },
              },
            },
          },
        },
      },
      ...meta
    });

    return {
      data: subscriptions,
      meta: {
        total: total_subscriptions,
        page: paginationDto?.page,
        pageSize: paginationDto?.pageSize,
        totalPages: paginationDto?.pageSize
          ? Math.ceil(total_subscriptions / paginationDto?.pageSize)
          : undefined,
      },
    };
  }

  async findOne(id: UUID) {
    const subscription = await this.prisma.subscriptions.findUnique({
      where: {
        id,
      },
      include: {
        package: true,
        transaction: {
          select: {
            aggregator: {
              select: {
                id: true,
                name: true,
              },
            },
            price: true,
            status: true,
            date: true,
            user: {
              select: {
                id: true,
                email: true,
                name: true,
                nickname: true,
                phoneNumber: true,
                remainingCredit: true,
                UserType: {
                  select: {
                    id: true,
                    name: true,
                  },
                },
              },
            },
          },
        },
      },
    });
    if (!subscription) {
      throw new HttpException('Subscription not found', HttpStatus.NOT_FOUND);
    }
    return subscription;
  }

  update(id: UUID, updateSubscriptionDto: UpdateSubscriptionDto) {
    if (Object.keys(updateSubscriptionDto).length === 0) {
      throw new HttpException('No data provided', HttpStatus.BAD_REQUEST);
    }

    const findSubscription = this.findOne(id);

    if ('id' in findSubscription) {
      //TODO: Make subscription inactive if user.remainingCredit isn't greater than 0 after update
      return this.prisma.subscriptions.update({
        where: {
          id,
        },
        data: {
          ...updateSubscriptionDto,
        },
        include: {
          package: true,
          transaction: {
            select: {
              aggregator: {
                select: {
                  id: true,
                  name: true,
                },
              },
              price: true,
              status: true,
              date: true,
              user: {
                select: {
                  id: true,
                  email: true,
                  name: true,
                  nickname: true,
                  phoneNumber: true,
                  remainingCredit: true,
                  UserType: {
                    select: {
                      id: true,
                      name: true,
                    },
                  },
                },
              },
            },
          },
        },
      });
    }
  }

  remove(id: UUID) {
    const findSubscription = this.findOne(id);

    if (!findSubscription) {
      throw new NotFoundException('Subscription not found');
    }

    return this.prisma.subscriptions.update({
      where: {
        id,
      },
      data: {
        isActive: false,
        deletedAt: new Date(),
      },
    });
  }
}
