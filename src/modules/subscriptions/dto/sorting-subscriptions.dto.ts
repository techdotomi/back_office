import { IsIn, IsOptional, IsString } from 'class-validator';
import { SortingDto } from 'src/common/dto/pagination.dto';

export class SortingSubscriptionsDto extends SortingDto {
  @IsOptional()
  @IsString()
  @IsIn([
    'packageId',
    'transactionId',
    'remainingCredit',
    'isActive',
    'deletedAt',
    'createdAt',
    'updatedAt',
  ])
  sortBy?: string = 'createdAt';
}
