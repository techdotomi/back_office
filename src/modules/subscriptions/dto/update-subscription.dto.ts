import { ApiProperty, PartialType } from '@nestjs/swagger';
import { CreateSubscriptionDto } from './create-subscription.dto';
import { IsOptional, IsString, Validate } from 'class-validator';
import { Type } from 'class-transformer';
import { TransactionIsCompletedOrUsedRule } from 'src/modules/transactions/dto/validation-rules/transaction-is-completed-or-used.rule';
import { PackageIdExistsRule } from 'src/modules/packages/dto/validation-rules/package-id-exists.rule';

export class UpdateSubscriptionDto extends PartialType(CreateSubscriptionDto) {
  @IsOptional()
  @IsString()
  @ApiProperty({
    name: 'transactionId',
    description: 'transaction of this subscription',
  })
  @Validate(TransactionIsCompletedOrUsedRule)
  transactionId: string;

  @IsOptional()
  @Type(() => Boolean)
  @ApiProperty({
    name: 'isActive',
    description: 'is this subscription active?',
  })
  isActive: boolean;

  @IsOptional()
  @IsString()
  @Validate(PackageIdExistsRule)
  @ApiProperty({
    name: 'packageId',
    description: 'package of this subscription',
  })
  packageId: string;
}
