import { ApiProperty } from '@nestjs/swagger';
import { Subscriptions } from '@prisma/client';
import { Type } from 'class-transformer';
import { IsNotEmpty, IsString, Validate } from 'class-validator';
import { PackageIdExistsRule } from 'src/modules/packages/dto/validation-rules/package-id-exists.rule';
import { TransactionIsCompletedOrUsedRule } from 'src/modules/transactions/dto/validation-rules/transaction-is-completed-or-used.rule';

export class CreateSubscriptionDto
  implements
    Omit<
      Subscriptions,
      'id' | 'deletedAt' | 'createdAt' | 'remainingCredit' | 'updatedAt'
    >
{
  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    name: 'transactionId',
    description: 'transaction of this subscription',
  })
  @Validate(TransactionIsCompletedOrUsedRule)
  transactionId: string;

  @IsNotEmpty()
  @Type(() => Boolean)
  @ApiProperty({
    name: 'isActive',
    description: 'is this subscription active?',
  })
  isActive: boolean;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    name: 'packageId',
    description: 'package of this subscription',
  })
  @Validate(PackageIdExistsRule)
  packageId: string;
}
