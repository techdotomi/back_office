import { Module } from '@nestjs/common';
import { NewsService } from './news.service';
import { NewsController } from './news.controller';
import { FileUploadModule } from '../file_upload/file_upload.module';

import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { generateRandomString } from 'src/libs/utils';
import { extname } from 'path';

@Module({
  controllers: [NewsController],
  providers: [NewsService],
  imports: [
    FileUploadModule,
    MulterModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        storage: diskStorage({
          destination: configService.get<string>('UPLOAD_PATH'),
          filename: (req, file, cb) => {
            const filename: string = `${file.fieldname}-${Date.now()}-${generateRandomString(10)}${extname(file.originalname)}`;
            cb(null, filename);
          },
        }),
      }),
      inject: [ConfigService],
    }),
  ],
})
export class NewsModule {}
