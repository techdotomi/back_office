import { News } from '@prisma/client';
import { ApiProperty } from '@nestjs/swagger';

export class NewsEntity implements News {
  @ApiProperty()
  id: string;

  @ApiProperty()
  readingNumber: number;

  @ApiProperty()
  newsTypeId: string;

  @ApiProperty()
  authorId: string;

  @ApiProperty()
  title: string;

  @ApiProperty()
  description: string | null;

  @ApiProperty()
  coverPicture: string | null;

  @ApiProperty()
  media: string | null;

  @ApiProperty()
  visible: boolean;

  @ApiProperty()
  validated: boolean;

  @ApiProperty()
  active: boolean;

  @ApiProperty()
  deletedAt: Date | null;

  @ApiProperty()
  createdAt: Date;

  @ApiProperty()
  updatedAt: Date;
}
