import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseFilePipeBuilder,
  Patch,
  Post,
  Query,
  Req,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { NewsService } from './news.service';
import { CreateNewsDto } from './dto/create-news.dto';
import { UpdateNewsDto } from './dto/update-news.dto';
import { ApiBearerAuth, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { UUID } from 'crypto';
import { NewsEntity } from './entities/news.entity';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { FileUploadService } from '../file_upload/file_upload.service';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { Request } from 'express';
import { PaginationDto } from 'src/common/dto/pagination.dto';
import { SortingNewsDto } from './dto/sorting-news.dto';

@Controller('news')
@ApiTags('news')
export class NewsController {
  constructor(
    private readonly newsService: NewsService,
    private readonly fileUploadService: FileUploadService,
  ) {}

  @Post()
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse({
    type: NewsEntity,
    description: 'Create News',
  })
  @UseInterceptors(
    FileFieldsInterceptor([
      { name: 'coverPicture', maxCount: 1 },
      { name: 'media', maxCount: 20 },
    ]),
  )
  async create(
    @UploadedFiles() // new ParseFilePipeBuilder()
    files // .addFileTypeValidator({
    //   fileType: 'jpeg',
    // })
    // .addMaxSizeValidator({
    //   maxSize: 1_000_000, // Max size in bytes
    // })
    // .build({
    //   fileIsRequired: false,
    // }),
    : {
      coverPicture?: Express.Multer.File[];
      media?: Express.Multer.File[];
    },
    @Body() createNewsDto: CreateNewsDto,
    @Req() req: Request,
  ) {
    if (!files) {
      throw new BadRequestException([
        {
          field: 'coverPicture',
          message: 'coverPicture are required',
        },
      ]);
    }
    if (!('coverPicture' in files)) {
      throw new BadRequestException([
        {
          field: 'coverPicture',
          message: 'coverPicture are required',
        },
      ]);
    }

    if (files.coverPicture && files.coverPicture.length > 1) {
      throw new BadRequestException([
        {
          field: 'coverPicture',
          message: 'coverPicture must be a single file',
        },
      ]);
    }

    const fileNames = await this.fileUploadService.uploadFiles(files);
    if (fileNames.coverPicture) {
      createNewsDto.coverPicture = fileNames.coverPicture[0];
    }
    if (fileNames.media) {
      createNewsDto.media = fileNames.media.join(',');
    }
    return await this.newsService.create(createNewsDto, req);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  findAll(
    @Query() paginationDto: PaginationDto,
    @Query() sortingDto: SortingNewsDto,
  ) {
    return this.newsService.findAll(paginationDto, sortingDto);
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  findOne(@Param('id') id: string) {
    return this.newsService.findOne(id as UUID);
  }

  @Patch(':id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  @ApiOkResponse({
    type: NewsEntity,
    description: 'Update News',
  })
  @UseInterceptors(
    FileFieldsInterceptor([
      { name: 'coverPicture', maxCount: 1 },
      { name: 'media', maxCount: 1 },
    ]),
  )
  async update(
    @Param('id') id: string,
    @UploadedFiles(
      new ParseFilePipeBuilder()
        // .addFileTypeValidator({
        //   fileType: 'jpeg|jpg|png|webp',
        // })
        // .addMaxSizeValidator({
        //   maxSize: 10000,
        // })
        .build({
          fileIsRequired: false,
        }),
    )
    files: {
      coverPicture?: Express.Multer.File[];
      media?: Express.Multer.File[];
    },
    @Body() updateNewsDto: UpdateNewsDto,
  ) {
    const fileNames = files
      ? await this.fileUploadService.uploadFiles(files)
      : undefined;
    if (fileNames) {
      if (fileNames.coverPicture) {
        updateNewsDto.coverPicture = fileNames.coverPicture[0];
      }
      if (fileNames.media) {
        updateNewsDto.media = fileNames.media.join(',');
      }
    }
    return this.newsService.update(id as UUID, updateNewsDto);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  remove(@Param('id') id: string) {
    return this.newsService.remove(id as UUID);
  }
}
