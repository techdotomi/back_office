import { IsIn, IsOptional, IsString } from 'class-validator';
import { SortingDto } from 'src/common/dto/pagination.dto';

export class SortingNewsDto extends SortingDto {
  @IsOptional()
  @IsString()
  @IsIn([
    'id',
    'readingNumber',
    'newsTypeId',
    'authorId',
    'title',
    'description',
    'coverPicture',
    'media',
    'visible',
    'validated',
    'active',
    'deletedAt',
    'createdAt',
    'updatedAt',
  ])
  sortBy?: string = "createdAt";
}
