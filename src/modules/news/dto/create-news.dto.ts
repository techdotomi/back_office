import { ApiProperty } from '@nestjs/swagger';
import { News } from '@prisma/client';
import {
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
  Validate,
} from 'class-validator';
import { NewsTypeIdExistsRule } from 'src/modules/news_types/dto/validation-rules/news_type-id-exists.rule';

export class CreateNewsDto
  implements
    Omit<
      News,
      | 'id'
      | 'authorId'
      | 'deletedAt'
      | 'createdAt'
      | 'updatedAt'
      | 'visible'
      | 'validated'
      | 'active'
      | 'readingNumber'
    >
{
  @IsNotEmpty()
  @IsUUID()
  @Validate(NewsTypeIdExistsRule)
  @ApiProperty({
    name: 'newsTypeId',
    description: 'The newsTypeId of the news',
  })
  newsTypeId: string;

  @IsNotEmpty()
  @IsString()
  @ApiProperty({
    name: 'title',
    description: 'The title of the news',
  })
  title: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    name: 'description',
    description: 'The description of the news',
  })
  description: string | null;

  @IsOptional()
  @IsString()
  @ApiProperty({
    name: 'coverPicture',
    description: 'The coverPicture of the news',
  })
  coverPicture: string | null;

  @IsOptional()
  @IsString()
  @ApiProperty({
    name: 'media',
    description: 'The media of the news',
  })
  media: string | null;
}
