import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import {
  IsBoolean,
  IsOptional,
  IsString,
  IsUUID,
  Validate,
} from 'class-validator';
import { NewsTypeIdExistsRule } from 'src/modules/news_types/dto/validation-rules/news_type-id-exists.rule';
import { PartialType } from '@nestjs/swagger';
import { CreateNewsDto } from './create-news.dto';

export class UpdateNewsDto extends PartialType(CreateNewsDto) {
  @IsOptional()
  @IsUUID()
  @Validate(NewsTypeIdExistsRule)
  @ApiProperty({
    name: 'newsTypeId',
    description: 'The newsTypeId of the news',
  })
  newsTypeId: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    name: 'title',
    description: 'The title of the news',
  })
  title: string;

  @IsOptional()
  @IsString()
  @ApiProperty({
    name: 'description',
    description: 'The description of the news',
  })
  description: string | null;

  @IsOptional()
  @IsString()
  @ApiProperty({
    name: 'coverPicture',
    description: 'The coverPicture of the news',
  })
  coverPicture: string | null;

  @IsOptional()
  @IsString()
  @ApiProperty({
    name: 'media',
    description: 'The media of the news',
  })
  media: string | null;

  @IsOptional()
  @Transform(({ value }) => (value.toString() === 'true' ? true : false))
  @IsBoolean()
  @ApiProperty({
    name: 'visible',
    description: 'The visible of the news',
  })
  visible: boolean;

  @IsOptional()
  @Transform(({ value }) => (value.toString() === 'true' ? true : false))
  @IsBoolean()
  @ApiProperty({
    name: 'validated',
    description: 'The validated of the news',
  })
  validated: boolean;

  @IsOptional()
  @Transform(({ value }) => (value.toString() === 'true' ? true : false))
  @IsBoolean()
  @ApiProperty({
    name: 'active',
    description: 'The active of the news',
  })
  active: boolean;
}
