import {
  BadRequestException,
  HttpStatus,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { CreateNewsDto } from './dto/create-news.dto';
import { UpdateNewsDto } from './dto/update-news.dto';
import { PrismaService } from 'src/libs/prisma/prisma.service';
import { UUID } from 'crypto';
import { FileUploadService } from '../file_upload/file_upload.service';
import { Request } from 'express';
import { User } from '../users/entities/user.entity';
import { PaginationDto } from 'src/common/dto/pagination.dto';
import { SortingNewsDto } from './dto/sorting-news.dto';

@Injectable()
export class NewsService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly fileUploadService: FileUploadService,
  ) {}

  private readonly logger = new Logger(NewsService.name);

  async create(createNewsDto: CreateNewsDto, req: Request) {
    this.logger.log(`Creating news with title ${createNewsDto.title}`);
    const news = await this.prisma.news.create({
      data: {
        ...createNewsDto,
        readingNumber: 0,
        authorId: (req.user as User).id,
      },
      include: {
        type: true,
        author: {
          select: {
            id: true,
            name: true,
            firstname: true,
            UserType: {
              select: {
                name: true,
              },
            },
          },
        },
      },
    });
    if (news) {
      this.logger.log(
        `News ${news.id} with title ${news.title} has been created`,
      );
    }
    return news;
  }

  async findAll(paginationDto?: PaginationDto, sortingDto?: SortingNewsDto) {

    const meta = {
      take: paginationDto?.pageSize,
      skip: paginationDto?.pageSize && paginationDto?.page? ((paginationDto?.page) - 1) * (paginationDto?.pageSize): undefined,
      orderBy: {
        [sortingDto?.sortBy || 'createdAt']: sortingDto?.sortOrder || 'desc',
      },
    }

    const total_news = await this.prisma.news.count();

    const news = await this.prisma.news.findMany({
      include: {
        type: true,
        author: {
          select: {
            id: true,
            name: true,
            firstname: true,
            UserType: {
              select: {
                name: true,
              },
            },
          },
        },
      },
      ...meta
    });

    return {
      data: news,
      meta: {
        total: total_news,
        page: paginationDto?.page,
        pageSize: paginationDto?.pageSize,
        totalPages: paginationDto?.pageSize? Math.ceil(total_news / (paginationDto?.pageSize)): undefined,
      }
    }
  }

  async findOne(id: UUID) {
    const news = await this.prisma.news.findUnique({
      where: {
        id: id,
      },
      include: {
        type: true,
        author: {
          select: {
            id: true,
            name: true,
            firstname: true,
            UserType: {
              select: {
                name: true,
              },
            },
          },
        },
      },
    });
    if (!news) {
      throw new NotFoundException('News with this id does not exist');
    }

    return news;
  }

  async update(id: UUID, updateNewsDto: UpdateNewsDto) {
    // Check if updateNewsDto have one or more attributes
    if (Object.keys(updateNewsDto).length === 0) {
      throw new BadRequestException('At least one attribute is required');
    }

    const findNews = await this.findOne(id);

    if (updateNewsDto.media && findNews.media) {
      this.logger.log(`Trying to delete ${findNews.media}`);
      for (const media of findNews.media.split(',')) {
        await this.fileUploadService.deleteFile(media);
      }
    }
    if (updateNewsDto.coverPicture && findNews.coverPicture) {
      this.logger.log(`Trying to delete ${findNews.coverPicture}`);
      await this.fileUploadService.deleteFile(findNews.coverPicture);
    }

    const news = await this.prisma.news.update({
      where: {
        id: id,
      },
      data: {
        ...updateNewsDto,
      },
      include: {
        type: true,
        author: {
          select: {
            id: true,
            name: true,
            firstname: true,
            UserType: {
              select: {
                name: true,
              },
            },
          },
        },
      },
    });

    if (!news) {
      throw new BadRequestException('Error during update news');
    }

    if (news) this.logger.log(`News with id ${id} has been updated`);
    return news;
  }

  async remove(id: UUID) {
    const findNews = await this.findOne(id);

    if ('id' in findNews) {
      if (findNews.media) {
        this.logger.log(`Trying to delete ${findNews.media}`);
        for (const media of findNews.media.split(',')) {
          await this.fileUploadService.deleteFile(media);
        }
      }
      if (findNews.coverPicture) {
        this.logger.log(`Trying to delete ${findNews.coverPicture}`);
        await this.fileUploadService.deleteFile(findNews.coverPicture);
      }
      await this.prisma.news.delete({
        where: {
          id,
        },
        include: {
          type: true,
          author: {
            select: {
              id: true,
              name: true,
              firstname: true,
              UserType: {
                select: {
                  name: true,
                },
              },
            },
          },
        },
      });

      this.logger.log(`News with id ${id} has been deleted`);

      return {
        message: `News with id ${id} has been deleted`,
        status: HttpStatus.OK,
      };
    } else {
      return findNews;
    }
  }
}
