import { Module } from '@nestjs/common';
import { MessageService } from './message.service';
import { MessageController } from './message.controller';
import { PrismaClient } from '@prisma/client';
import { PrismaModule } from 'src/libs/prisma/prisma.module';

@Module({
  controllers: [MessageController],
  providers: [MessageService],
  imports: [PrismaClient, PrismaModule],
  exports: [MessageService],
})
export class MessageModule {}
