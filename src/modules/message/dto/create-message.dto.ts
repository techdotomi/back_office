import { BadRequestException } from '@nestjs/common';
import { Messages, MessageType } from '@prisma/client';
import { Transform, Type, TypeHelpOptions } from 'class-transformer';
import {
  Equals,
  IsDefined,
  IsNotEmpty,
  IsOptional,
  IsNotEmptyObject,
  IsString,
  IsUrl,
  IsUUID,
  Validate,
  ValidateNested,
} from 'class-validator';
import { ChatRoomIdExistsRule } from 'src/modules/chat_room/dto/validation-rules/chat_room-id-exists';
import { UserIdExistsRule } from 'src/modules/users/dto/validation-rules/user-id-exists.rule';

export class CreateTextMessageDto
  implements
    Omit<
      Messages,
      'id' | 'mediaUrl' | 'chatRoomId' | 'senderId' | 'createdAt' | 'updatedAt'
    >
{
  @Transform(({ value }) => value.toUpperCase() as MessageType)
  @Equals(MessageType.TEXT)
  type: MessageType;

  @IsNotEmpty()
  @IsString()
  content: string;
}

export class CreateMediaMessageDto
  implements
    Omit<
      Messages,
      'id' | 'content' | 'chatRoomId' | 'senderId' | 'createdAt' | 'updatedAt'
    >
{
  @Transform(({ value }) => value.toUpperCase() as MessageType)
  @Equals(MessageType.IMAGE || MessageType.VIDEO || MessageType.AUDIO)
  type: MessageType;

  @IsOptional()
  @IsUrl()
  mediaUrl: string;
}

export class CreateMessageDto {
  @IsNotEmpty()
  @IsUUID()
  @Validate(ChatRoomIdExistsRule)
  chatRoomId: string;

  @IsNotEmpty()
  @IsUUID()
  @Validate(UserIdExistsRule)
  senderId: string;

  @IsNotEmptyObject()
  @IsDefined()
  @Type((data: TypeHelpOptions) => {
    switch (data.object.message.type) {
      case MessageType.TEXT:
        return CreateTextMessageDto;
      case MessageType.AUDIO || MessageType.IMAGE || MessageType.VIDEO:
        return CreateMediaMessageDto;
      default:
        throw new BadRequestException({
          field: 'message',
          message: 'Invalid message type',
        });
    }
  })
  @ValidateNested()
  message: CreateTextMessageDto;
}
