import { Injectable, Logger } from '@nestjs/common';
import {
  CreateMediaMessageDto,
  CreateMessageDto,
  CreateTextMessageDto,
} from './dto/create-message.dto';
import { PrismaService } from 'src/libs/prisma/prisma.service';
import { MessageType } from '@prisma/client';
import { WsException } from '@nestjs/websockets';

@Injectable()
export class MessageService {
  private readonly logger = new Logger(MessageService.name);

  constructor(private readonly prisma: PrismaService) {}

  async create(createMessageDto: CreateMessageDto) {
    try {
      const message = await this.prisma.messages.create({
        data: {
          chatRoomId: createMessageDto.chatRoomId,
          senderId: createMessageDto.senderId,
          type: createMessageDto.message.type,
          content:
            createMessageDto.message.type === MessageType.TEXT
              ? (createMessageDto.message as CreateTextMessageDto).content
              : undefined,
          mediaUrl: undefined,
          // createMessageDto.message.type === MessageType.IMAGE ||
          // MessageType.VIDEO ||
          // MessageType.AUDIO
          //   ? (createMessageDto.message as CreateMediaMessageDto).mediaUrl
          //   : undefined,
        },
        include: {
          sender: {
            select: {
              id: true,
              name: true,
              firstname: true,
              nickname: true,
              phoneNumber: true,
              sex: true,
              avatarPicture: true,
              email: true,
              online: true,
              UserType: true,
            },
          },
        },
      });

      this.logger.log(
        `Message created successfully by User ID: ${message.senderId}`,
      );

      return message;
    } catch (error) {
      this.logger.error(
        `Failed to create message by User ID: ${createMessageDto.senderId}. Error: ${error.message}`,
        error.stack,
      );
      throw new WsException(
        'An error occurred while creating the message. Please try again.',
      );
    }
  }

  // findAll() {
  //   return `This action returns all message`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} message`;
  // }

  // update(id: number, updateMessageDto: UpdateMessageDto) {
  //   return `This action updates a #${id} message`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} message`;
  // }
}
